#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
CachedCalculator class

Author: Nikolai G. Lehtinen
"""

import os
import numpy as np
from utils import red

####################################################################################################
#%% A base class for cached calculations
# Things to do: remote control

class CachedCalculator:
    """Persistent object for caching calculations.
    We want it to be flexible so that it can load/save files when needed.
    
    Example of usage:
    -----------------
    >>> import time
    ... class dumb_calculator(CachedCalculator):
    ...     "These two functions must have the same signatures!"
    ...     def file_name(self,x,progress=False):
    ...         return 'dumb'+str(x)
    ...     def calculate(self,x,progress=False):
    ...         if progress:
    ...             for k in range(10):
    ...                 print('X',end='',flush=True)
    ...                 time.sleep(1.0)
    ...             print(' ... finished!',end='',flush=True)
    ...         return {'y':x**2,'z':'az'}
    ... f = dumb_calculator(one_liner=False) # for very long calculations, displaying lot out output
        
    It can then be used in the following manner:
    
    >>> x = f.verbose.nocache(5,progress=True) # acts as a regular function, do not read file
    >>> y = f.verbose.noload(5) # creates a file 'dumb5.npz' but returns None if it already exists
    >>> z = f.verbose(5) # regular cached calculation
    >>> t = f.verbose.erase(5) # recalculate
    >>> u = f.verbose.nocalc(5) # if the file does not exist, return None, otherwise read from file
        
    It is possible to do without subclassing:
        
    >>> f = CachedCalculator() 
    >>> f.file_name = lambda x: 'dumb'+str(x)
    >>> f.calculate = lambda x: {'y':x**2,'z':'az'}
    
    and use it in exactly the same manner as above.
    """
    def __init__(self,one_liner=True):
        self.one_liner = one_liner
        self.__restore()
    "The following two functions are vitual and must be overloaded and must have same signatures."
    def file_name(self,*args,**kws):
        raise NotImplementedError('"file_name" is not defined!')
    def calculate(self,*args,**kws):
        "Implementation is responsible for displaying progress if self.progress==True"
        raise NotImplementedError('"calculate" is not defined!')
    "Several functions that change the defaults"
    @property
    def erase(self):
        "Erase the existing cache. Should be used only if calculation algorithm changed."
        self.__erase = True # erase existing cache (force recalculation)
        return self
    @property
    def noload(self):
        "Skip loading the file if it exists, but calculate and create the cache otherwise"
        self.__noload = True # if file exists, just skip it (do not return anything)
        return self
    @property
    def nocache(self):
        "Make it behave just like a regular function"
        self.__nocache = True
        return self
    @property
    def verbose(self):
        self.__verbose = True
        return self
    def lock(self,s):
        self.__lock = s
        return self
    @property
    def nocalc(self):
        self.__nocalc = True
        return self
    def __restore(self):
        "Defaults"
        self.__erase = False # do not erase existing cache
        self.__noload = False # do not skip loading data from file
        self.__nocache = False
        self.__verbose = False
        self.__lock = 'lock'
        self.__nocalc = False
    def __return(self,result=None):
        self.__restore()
        return result
    def __call__(self,*args,**kws):
        """This class is written for the sake of this wrapper."""
        prefix = self.file_name(*args,**kws)
        if not self.__nocache:
            fname = prefix + '.npz'
            flock = prefix + '.' + self.__lock
            if os.path.isfile(flock):
                if self.__verbose:
                    print('Skipping locked',fname)
                return self.__return()
            if not self.__erase and os.path.isfile(fname):
                if self.__verbose:
                    print('Not calculating',fname,end=' ')
                if self.__noload:
                    # skip loading the file
                    if self.__verbose: print('(not loading)')
                    result = None
                else:
                    if self.__verbose: print('(loading)')
                    result = dict(np.load(fname,allow_pickle=True))
                return self.__return(result)
            if self.__nocalc:
                if self.__erase:
                    print(red('Warning:'),'incompatible options erase and nocalc, ignoring erase')
                # File not found
                if self.__verbose:
                    print('File not found:',fname)
                return self.__return()
        # Now, we have to do calculations
        if self.__nocache:
            if self.__noload and self.__verbose:
                print(red('Warning:'),'ignoring noload option')
            message=red('(not saving!)')
        else:
            with open(flock, 'w') as f:
                f.write('')
            message=''
        print('Calculating',prefix,message,'...',end=(' ' if self.one_liner else '\n'))
        result = self.calculate(*args,**kws) # all calculation is done here
        for k,v in result.items():
            result[k] = np.asarray(v)
        if not self.__nocache:
            np.savez(fname,**result)
            os.remove(flock)
        if self.one_liner:
            print('done!')
        else:
            print('')
        return self.__return(result)
    pass

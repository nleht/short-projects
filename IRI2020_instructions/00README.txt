##############################################################################
# New instructions for IRI model (IRI2020)
# Date: 2023-02-14
# Environment Versions: Python 3.10.6, gcc 11.3.0, Ubuntu 22.04.1 LTS

##### WARNING!!!: DO NOT install 'pyglow' as the IRI website suggests. It is a completely unrelated package (at PyPI)!

##### Download and unzip IRI2020 files (see explanation at http://irimodel.org/)
mkdir IRI # the root dir for all IRI stuff
cd IRI
# 1. Source files
wget -c http://irimodel.org/IRI-2020/00_iri.zip
unzip 00_iri.zip # has both code and data, creates directory IRI-2020
rm -f 00_iri.zip
cd IRI-2020
mkdir data
mv *.dat data/
mkdir src
mv *.for src/

# 2. Common files
wget -P COMMON_FILES -c http://irimodel.org/COMMON_FILES/00_ccir-ursi.tar
cd COMMON_FILES && tar xvf 00_ccir-ursi.tar && cd ..

# 3. Indices
rm -rf indices
# Choose one:
# Indices (updated daily)
wget -P indices -c https://chain-new.chain-project.net/echaim_downloads/apf107.dat
wget -P indices -c https://chain-new.chain-project.net/echaim_downloads/ig_rz.dat
# Indices (updated twice a year)
wget -P indices -c http://irimodel.org/indices/apf107.dat
wget -P indices -c http://irimodel.org/indices/ig_rz.dat

##### Apply the patch and fix the data directories
# Bug fix + fixed directories for IRI data
# Patch was produced by 'diff -Naur src src_new > IRI2020.patch'
patch -p0 < IRI2020.patch
# Replace the directory with YOURS. For example,
cd src
sed -i 's/$IRI_DIR/\/scratch2\/python\/venvs\/work\/IRI/g' igrf.for irifun.for irisub.for

##### Create a static library and install the library and header files
# We are already in 'src'
gfortran -fPIC -w -ffixed-line-length-none -ffree-line-length-none -g -c cira.for igrf.for iridreg.for iriflip.for irifun.for irirtam.for irisub.for iritec.for rocdrift.for
ar crUus ../libIRI2020.a *.o
rm -f *.o
cd ..
# Place the created libIRI2020.a in the static library search path. This replaces -L option of the compiler:
export LIBRARY_PATH=$HOME/.local/lib # or $VIRTUAL_ENV/lib to be used in a venv
# Place the attached file IRI2020f.h in the header search path. This replaces -I option of the compiler:
export CPATH=$HOME/.local/include # or $VIRTUAL_ENV/include to be used in a venv
# These commands should be in your .bashrc file (or 'activate' to be used in a venv)

##### Test in C
# The file iri_simpletest.c is attached.
gcc -Wall -g iri_simpletest.c -lIRI2020 -lgfortran -lm -o iri_simpletest
./iri_simpletest

##### Instructions for Python3
# Make sure that instructions for C work first.
# *.pyf are needed only from files irisub.for, irifun.for, igrf.for and only for functions iri_sub, read_ig_rz, readapf107, igrf.
# *.pyf may be created e.g. by "f2py -h irisub.pyf irisub.for -m IRI2020f2py"
# Thus, IRI2016f2py_i.pyf was composed of several *.pyf files
# We also changed the dimensionality of 2D arrays to 1D (it looks like there is a bug which does not allow 2D arrays) and got rid of all common blocks (we don't need access to them)
# Appropriate variables were chosen as inputs and output using "intent(in)" or "intent(out)" (see http://docs.scipy.org/doc/numpy-dev/f2py/getting-started.html)

# Create the .so file for Python:
f2py -c IRI2020f2py_i.pyf -lIRI2020 -lgfortran # use -L option if library is not found
# Place the created file IRI2020f2py.cpython-*.so and attached IRI2020py.py onto Python3 package search path (e.g., $VIRTUAL_ENV/lib/python*/dist-packages/)
mv IRI2020f2py.cpython-310-x86_64-linux-gnu.so ~/lib/python3.10/dist-packages/
cp IRI2020py.py ~/lib/python3.10/dist-packages/

# Test its work -- run iri_pytest.py


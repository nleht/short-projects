# International Reference Ionosphere

We provide instructions to use the Fortran code for IRI-2020 model of the ionosphere with C and Python. The extended instructions, which include creation of a dynamic library and instructions for MATLAB, are available for the previous version (IRI-2016) in the [attachment](https://gitlab.com/nleht/stanfordfwm/-/tree/main/external) to the [StanfordFWM](https://gitlab.com/nleht/stanfordfwm/) model.


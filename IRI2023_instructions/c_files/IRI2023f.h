#ifndef IRI2023F_H
#define IRI2023F_H

/* Some definitions from f2c.h */
typedef int integer;
typedef float real;
typedef int logical;

/* External Subroutines */
/* They use common blocks! */
int read_ig_rz_();
int readapf107_();
int iri_sub_(
	/* inputs */
	logical *a_jf, integer *a_jmag, real *alati, real *along, integer *iyyyy, integer *mmdd, real *dhour,
	real *heibeg, real *heiend, real *heistp,
	/* outputs */
	real *a_outf, real *a_oar);
int iri_web_(
	/*inputs*/
	integer *a_jmag, logical *a_jf,
	real *xlat, real *xlon, integer *iy, integer *mmdd, integer *iut, real *hour,
	real *hxx, real *htec_max, integer *ivar, real *vbeg, real *vend, real *vstp,
	/*outputs*/
	real *a_outf, real *a_oar);
int feldcof_(real *year);
int feldg_(real *xlat, real *xlon, real *height, real *bnorth, real *beast, real *bdown, real *babs);
int setrot_(char *rtdir, logical *debug, int len);

#endif /* IRI2023F_H */


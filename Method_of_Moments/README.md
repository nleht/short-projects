# Method of Moments (MoM)

This package allows to calculate electric charge distribution, electrostatic field and capacitance of a cylindrically-symmetric rod or a collection of rods (of any cylinrically-symmetric shape), with given charges on the separate pieces. It also allows to re-calculate outputs after some pieces are connected together by a wire, as a result of which their elctrostatic potentials become equal and the charges get redistributed.

See examples for usage details.

If you would like to use it, cite the following paper:
* Skeltved, A. B., N. Østgaard, A. Mezentsev, N. Lehtinen, and B. Carlson (2017), Constraints to do realistic modeling of the electric
field ahead of the tip of a lightning leader, _J. Geophys. Res. Atmos._, **122**, 8120–8134, [doi:10.1002/2016JD026206](https://dx.doi.org/10.1002/2016JD026206).

If you use it for multiple conductors (see the "partitioning" theory below), cite also:
* Skeie, C. A., Østgaard, N., Lehtinen, N. G., Sarria, D., Kochkin, P., de Boer, A. I., et al. (2020). Constraints on recoil leader properties estimated from X-ray emissions in aircraft-triggered discharges. _J. Geophys. Res. Atmos._, **125**, e2019JD032151. [doi:10.1029/2019JD032151](https://doi.org/10.1029/2019JD032151)

# Theoretical background

## Notations

* Sans-serif font capital letters like A are matrices or operators.
* I<sub>K</sub> is a unit matrix (identity operator) of size *K* × *K*, where *K* is a positive integer.
* Vectors are usually bold like **a**, except Greek letters which are hard to render in bold.

## Discretization of the Poisson equation

A solution *ϕ* of Poisson equation ∇<sup>2</sup>*ϕ* = − *ρ*/*ε*<sub>0</sub> is represented as a convolution
```math
  \int G(\mathbf{r},\mathbf{r}')\rho(\mathbf{r}')\,d^3\mathbf{r}' = \phi(\mathbf{r}),\qquad G(\mathbf{r},\mathbf{r}')=\frac{1}{4\pi\epsilon_0|\mathbf{r}-\mathbf{r}'|}
```
of *ρ* with **Green's function** *G*. This is a **Fredholm integral equation of the first kind** which may be written symbolically as
```math
\begin{equation}
\mathsf{G}\rho=\phi
\end{equation}
```
where G is a linear operator and *ρ* and *ϕ* are functions.

The method of moments (MoM) solves it by discretization, i.e. G is represented as a matrix and *ρ* and *ϕ* as vectors. This is done in the following way:
* approximate the unknown function as a linear combination of **basis (expansion, shape)** functions *u* as *ρ*(**r**) ≈ *ρ*<sub>h</sub>(**r**) = ∑<sub>j</sub> *ρ*<sub>j</sub> *u*<sub>j</sub>(**r**). The approximating function *ρ*<sub>h</sub>(**r**) may be called a **trial** function, e.g., in [FEniCS](https://fenicsproject.org/);
* try to satisfy the above equation with a discrete number of conditions *ϕ*<sub>i</sub>=(*w*<sub>i</sub>, *ϕ*), where *w* are the **testing (weighting)** functions, and the scalar product is defined as (*f*, *g*) ≡ ∫ *f*(**r**) *g*(**r**) d<sup>3</sup>**r**.

Discretization leads to a system of linear equations ∑<sub>j</sub> *G*<sub>ij</sub> *ρ*<sub>j</sub> = *ϕ*<sub>i</sub>, where *G*<sub>ij</sub> ≡ (*w*<sub>i</sub>, G *u*<sub>j</sub>). For an exact solution, the number of *u* and *w* usually must be infinite. For a numerical solution, it is, of course, finite, and we have a **weak formulation**. If their number is the same, the linear system may be solved if the matrix *G*<sub>ij</sub> is invertible. Equation (1) now may also be understood as a discretized equation, in which G is a matrix and *ρ* and *ϕ* are vectors. It is convenient to have *u* and *w* have dimensions of inverse volume, then elements of vector *ρ* have dimensions of charge and elements of *ϕ* still have dimensions of electrostatic potential. Moreover, it is convenient to normalize functions *u* so that ∫ *u*<sub>j</sub>(**r**) d<sup>3</sup>**r** = 1, in this case *ρ*<sub>j</sub> is the charge of the *j*-th element, and to normalize functions *w* in the same way so that in the case of a constant potential, *ϕ*<sub>i</sub> = *ϕ*. The matrix G relates charges and potentials, thus has a physical meaning of an inverse capaciatance, C<sup>-1</sup> = G. Let us take  *N* finite elements, so that G has size *N* × *N*. 

Other terminology used in the **finite-element methods (FEM)** (this material is not related to our description and is given just for general reference):
* If *w*<sub>i</sub> ≡ *u*<sub>i</sub>, the FEM is (usually) called a **Galerkin** method (however, many authors just use the term "Galerkin method" for a general FEM).
* If *w*<sub>i</sub> are nonzero only on non-intersecting finite volumes which cover the whole simulation domain, such FEM are called **finite-volume methods (FVM)**. An important class of such methods are **discontinuous Galerkin methods (DGM)** which usually (for orders>1) have several *w*<sub>i</sub> corresponding to the same finite volume.
* (Especially in Galerkin methods) *M*<sub>ij</sub> ≡ (*w*<sub>i</sub>, *u*<sub>j</sub>) is sometimes called the **mass matrix**. For the efficiency of a FEM, the mass matrix should be as easily invertible as possible (e.g, diagonal, or, at worst, block-diagonal).
* See more discussion of FEM in [COMSOL](https://www.comsol.com/blogs/keeping-track-of-element-order-in-multiphysics-models/) and [FEniCS](https://fenicsproject.org/documentation/) packages.

## Solving the discretized equations with appropriate constraints

We solve (1) for a _system of conductors_. We know that the charges are located on conductor surfaces, and the charge may flow from one part of a conductor to another, but the total charge on a given conductor is conserved. It is also a known fact that the potentials of all surface points on the same conductor are equal. (This is, by the way, a consequence of minimizing the **energy functional** U = (*ρ*, G *ρ*)/2 in respect to unknown *ρ* with the charge conservation constraint.) Thus, we better choose both *u* and *w* to be small pieces of the conductor surfaces. How the charges redistribute themselves, is not known in advance, so both *ρ* and *ϕ* in equation (1) are unknown. There are a total of 2*N* unknowns. The system (1) gives *N* equations, so for a unique solution we need *N* extra equations (constraints).

The conductors may be immersed in external field **E**<sub>e</sub> = − ∇ *ϕ*<sub>e</sub>. E.g., a unit uniform field is represented by function *ϕ*<sub>e</sub> = − z, or a vector with elements (*ϕ*<sub>e</sub>)<sub>i</sub> = − z<sub>i</sub>. The total potential is *ϕ* = G *ρ* + *ϕ*<sub>e</sub>. We rewrite this equation as
```math
\begin{equation}
\mathsf{G}\rho-\phi=-\phi_e
\end{equation}
```
with all unknowns on the LHS.

The partitioning *N*<sub>p</sub> × *N* matrix P partitions *N* elements into *N*<sub>p</sub> connected pieces:
```math
\mathsf{P}=\left(\begin{array}{cccc|ccc|cc}
1 & 1 & 1 & 1 &   &   &   &   &   \\
\hline
  &   &   &   & 1 & 1 & 1 &   &   \\
\hline
  &   &   &   &   &   &   & 1 & 1 
\end{array}\right)
```

We may be given the following constraints:

1. Charges *Q*<sub>p</sub> on each *p*-th connected piece (where *p* = 1 ... *N*<sub>p</sub>) are known. Using the partitioning matrix, these charges may be represented as a vector
```math
\mathbf{Q}=\left(\begin{array}{c} Q_1 \\ \vdots \\ Q_P \end{array}\right) = \mathsf{P}\rho
```

2. Potentials of elements constituting each connected piece *p* are the same and equal to an uknown value *Φ*<sub>p</sub>. These potentials form a vector
```math
\mathbf{\Phi}=\left(\begin{array}{c} \Phi_1 \\ \vdots \\ \Phi_P \end{array}\right)
```
which is related to the vector *ϕ* of *N* unknown potentials as *ϕ* = P<sup>T</sup>**Φ**, which may be substituted into (2).

The obtained system
```math
\left\{\begin{array}{rcl}
\mathsf{G}\rho -\mathsf{P}^T\mathbf{\Phi} & = & -\phi_e \\
\mathsf{P}\rho & = & \mathbf{Q}
\end{array}\right.
```
has *N* + *N*<sub>p</sub> equations for *N* + *N*<sub>p</sub> unknowns *ρ*, **Φ** . It may also be written in a matrix form
```math
\begin{equation}
\left(\begin{array}{cc} \mathsf{G} & \mathsf{P}^T \\ \mathsf{P} & 0 \end{array}\right)
\left(\begin{array}{c} \rho \\ -\mathbf{\Phi} \end{array}\right) =
\left(\begin{array}{c} -\phi_e \\ \mathbf{Q} \end{array}\right)
\end{equation}
```
with a symmetric matrix on the LHS (if *u* and *w* are the same).

This matrix may be solved in the following way. The first equation gives *ρ* = G<sup>-1</sup>(− *ϕ*<sub>e</sub> + P<sup>T</sup>**Φ**). From now on, we may also use notation C ≡ G<sup>-1</sup>, as we noted before that G has the physical meaning of the inverse capacitance matrix. Substituting *ρ* into the second equation, we find **Φ** = (PCP<sup>T</sup>)<sup>-1</sup>(**Q** + PC*ϕ*<sub>e</sub>). This equation may be interpreted as a variant of (2) in which we substituted *ϕ* → **Φ**, C ≡ G<sup>-1</sup> → PCP<sup>T</sup>, *ϕ*<sub>e</sub> → (PCP<sup>T</sup>)<sup>-1</sup>PC *ϕ*<sub>e</sub>. Thus, we may rewrite (2) as
```math
\begin{equation}
\mathsf{G}_p\mathbf{Q}-\mathbf{\Phi}=-\mathbf{\Phi}_e
\end{equation}
```
where we introduce the notions of
* Partitioned operator G<sub>p</sub> whose inverse is the partitioned capacitance matrix C<sub>p</sub> = G<sub>p</sub><sup>-1</sup> = PCP<sup>T</sup>, which has the physical meaning of mutual capacitance between disconnected conducting pieces;
* Partitioned external potential **Φ**<sub>e</sub> = P<sub>ϕ</sub>*ϕ*<sub>e</sub>, which is calculated with the help of the partitioning matrix for potentials, P<sub>ϕ</sub> = G<sub>p</sub>PC. This matrix has the property P<sub>ϕ</sub>P<sup>T</sup> = I<sub>Np</sub> which may be checked by substitution. The partitioned external potential **Φ**<sub>e</sub> has the physical meaning of the zero-charge potential which is obtained when **Q** = 0.

With these notations, we have
```math
\mathbf{\Phi} = \mathsf{G}_p\mathbf{Q} + \mathsf{P}_\phi \phi_e
```
Finally, *ρ* may be found by substituting the found **Φ**:
```math
\rho = \mathsf{C}(− \mathsf{I}_N + \mathsf{P}^T \mathsf{P}_\phi) \phi_e + \mathsf{P}_\phi^T \mathbf{Q}
```
We see that the matrix P<sub>ϕ</sub> pops up also in another place: when *ϕ*<sub>e</sub> = 0, then *ρ* = P<sub>ϕ</sub><sup>T</sup>**Q**, where P<sub>ϕ</sub><sup>T</sup> = CP<sup>T</sup>G<sub>p</sub>, so it plays the role of "antipartitioning" the charge, analogously to *ϕ* = P<sup>T</sup> **Φ**.

We can repeat the procedure of "partitioning" which led from (2) to (4), with the physical meaning of connecting separate conductors with conducting wires. The charges will then redistribute themselves again.

## Using the code to calculate *ρ* etc.

A cylindrically symmetric conductor system is created with constructor `conductor = CylSymConductor(zb, rb)`, where `zb`, `rb` are z- and r-coordinates of boundaries between elements, with separate conductor pieces separated by NaNs. Use keyword `Nsub` to subdivide each interval into `Nsub` subintervals for more accurate calculations. The conductors can be electrically connected by specifying keyword `connect`, e.g. `connect=[0, 0, 1, 1, 1]` means the first two pieces are connected and so are the last three pieces. At these stage, the following fields are filled:
* Number of elements *N* = `conductor.N`
* Number of connected pieces *N*<sub>p</sub> = `conductor.Np`
* Partitioning matrix P = `conductor.Pmat`
* Centers of elements `conductor.zc`, `conductor.rc`
* Widths of elements in *z*, i.e. d*z* = `conductor.zw` (useful to calculate the linear charge density)
* Total widths of the elements = `conductor.d`
* Areas of elements = `conductor.area`
* Sine and cosine of the angle of each conical element `conductor.s`, `conductor.c`

Full Green operator G = `conductor.C_inv` is calculated using `conductor.fill_matrix()` (this step is optional because it is called automatically at the next stage anyway). We use the fact that for a thin ring of unit charge, radius *r*<sub>1</sub> and located at *z* = *z*<sub>1</sub>, the electrostatic potential at *r* = *r*<sub>2</sub> and *z* = *z*<sub>2</sub> is given by
```math
G(2,1)=\frac{1}{2\pi^2\epsilon_0\sqrt{A_+}}K\left(1-\frac{A_-}{A_+}\right),\qquad A_\pm = (z_2-z_1)^2 + (r_2 \pm r_1)^2
```
where *K*(*m*) is the *complete elliptic integral of the first kind*.

Next, `conductor.solve()` calculates the following:
* Transposed partitioning matrix for potentials P<sub>ϕ</sub><sup>T</sup> = `conductor.q_from_Q`, which can be used to calculate *ρ* = P<sub>ϕ</sub><sup>T</sup>**Q** for no external field.
* Partitioned Green operator G<sub>p</sub> = `conductor.Cp_inv` (also has the meaning of inverse mutual capacitance matrix for connected pieces)
* The partitioned external potential **Φ**<sub>e</sub> which has the physical meaning of the zero-charge potential which is obtained when **Q** = 0, for the unit external field `conductor.Phi_from_Ee`

The piece charges **Q** are specified by setting `conductor.Q` and the external uniform field E<sub>e</sub>=-∇*ϕ*<sub>e</sub> by setting `conductor.Ee` (by default, both are zero). Then, property *ρ* = `conductor.q` automatically returns the charge distribution, and property **Φ** = `conductor.Phi` the potentials of each piece.

The conductors connections between each other can be rearranged by method `conductor.short_circuit(connect)`, which reduces the number of connected pieces.

## Calculating the electric potential and field

The electric potential and field are calculated on the axis, for a "belt"-like element with uniform surface charge density. For the formulas and explanations, see comments in the corresponding functions. The user can use the following functions:
* Potential on the axis `conductor.pot_axis(z)`
* Electric field on the axis `conductor.E_axis(z)`

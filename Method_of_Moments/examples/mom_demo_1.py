#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 23 17:15:49 2018

NOTE: Some things may not work very smoothly, because this was created for
an older version of method_of_moments.py

The function "analyze_rod" was reimplemented (in a simpler version) as
MoM.rod_cap_field_enhancement_and_width()

@author: nle003
"""

#%% Preliminaries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import method_of_moments as MoM

def analyze_rod(L, config, verbose=0):
    "Create and analyze the conductor object: cylinder of radius 1 and tip-to-tip length 2*L"
    #%
    zb,rb = MoM.rod(L, config, dmin=0.02, dmax=.2, hole=1e-6, dxch=0.1, threfine=1, verbose=verbose)
    #zb,rb = MoM.rod(L,config,dmax=0.25,verbose=verbose)
    #Ndis = 3*np.ones((len(zb)-1,),dtype=np.int)
    conductor = MoM.CylSymConductor(zb, rb, Nsub=3, verbose=verbose)
    if verbose>0:
        print('Created a conductor with',conductor.N,'elements',flush=True)
    conductor.solve()
    # The typical linear charge density around the ends
    conductor.Ee = 1
    conductor.Q = np.array([0])
    lams = conductor.q_from_Ee/conductor.zw # unit field, not taking conductor.Ee into account
    i0 = np.argmin(np.abs(zb))
    lam1 = (lams[i0+1]-lams[i0-1])/(conductor.zc[i0+1]-conductor.zc[i0-1])*(L-1)
    ii = np.argmin(np.abs(zb - L + 1)) # at the neck
    lam2 = lams[ii]
    ii = np.argmin(np.abs(zb-L+2)) # at the neck
    if L>2:
        lam3 = lams[ii]
    else:
        lam3 = 0
    Nx=5000
    #zpb=np.linspace(L0/2-10,L0/2+10,Nx)
    x=np.linspace(0, 1, Nx)
    # The field is NOT including the external field!
    E = np.array([conductor.E_axis(x1+L) for x1 in x])
    assert(not any(np.isnan(E)))
    imax = np.nanargmax(E)
    eta = E[imax]
    Eat0 = E[0]
    locmax = x[imax]
    #%
    if config=='open':
        loc = locmax
    elif config=='cap':
        #% Fit the Naidis curve
        #front
        def Lehtinen(z,a):
            return 1/(1+z/a)**2
        def Combined(z,l,a,r):
            return r/(1+z/l)+(1-r)/(1+z/a)**2
        def Naidis(z,l):
            return 1/(1+z/l)
        xs = x[imax:]
        Es = E[imax:]
        assert not any(np.isnan(Es))
        assert not any(np.isnan(x))
        popt1,_ = curve_fit(Naidis, xs, Es/eta) #/(1+np.exp(-x[imax:]/0.1)))
        popt2,_ = curve_fit(Lehtinen, xs, Es/eta)
        popt,_ = curve_fit(Combined,xs,Es/eta)
        err1 = np.max(np.abs(Naidis(xs,*popt1)-Es/eta))
        err2 = np.max(np.abs(Lehtinen(xs,*popt2)-Es/eta))
        err = np.max(np.abs(Combined(xs,*popt)-Es/eta))
        #plt.plot(x,E) #/(1+np.exp(-x/0.1)))
        #plt.plot(x,Naidis(x,*popt1))
        #plt.plot(x,Lehtinen(x,*popt2))
        #%
        loc=(popt,popt1,popt2,err,err1,err2)
    else:
        loc = None
    Eat1 = E[-1]
    capac = 1./conductor.Cp_inv[0,0]
    return conductor,eta,capac,Eat0,Eat1,loc,lam1,lam2,lam3

#%% Checking eta_fun with method of moments
config = 'cap'
Larr = np.hstack((np.arange(1,50,0.5),np.arange(50,102,2))) # half-length
#Larr = np.arange(100,10000,500)
eta = np.zeros(Larr.shape)
capac = np.zeros(Larr.shape)
loc = np.zeros(Larr.shape)
E0 = np.zeros(Larr.shape)
E1 = np.zeros(Larr.shape)
lam1 = np.zeros(Larr.shape)
lam2 = np.zeros(Larr.shape)
lam3 = np.zeros(Larr.shape)
if config=='cap':
    stuff = []
for k,L in enumerate(Larr):
    conductor,eta1,capac1,E01,E11,loc1,lam11,lam21,lam31 = analyze_rod(L,config)
    print('Done L =',L,': N =',conductor.N,', eta =',eta1,', capacitance =',capac1)
    capac[k]=capac1
    eta[k]=eta1
    if config=='open':
        loc[k]=loc1
    elif config=='cap':
        stuff.append(loc1)
    E0[k]=E01
    E1[k]=E11
    lam1[k]=lam11
    lam2[k]=lam21
    lam3[k]=lam31

#%%
def eta_general1(La,a,b):
    "La/eta"
    return a + b*np.log(La)
popt,_ = curve_fit(eta_general1,Larr,Larr/eta)
plt.figure(1)
plt.clf()
plt.plot(Larr, eta,'x-')
plt.plot(Larr,Larr/eta_general1(Larr,*popt))
plt.plot(Larr,MoM.field_enhancement(Larr,'cap'))
plt.plot(Larr,MoM.field_enhancement_cap_Raizer(Larr))
plt.xlabel('L/a')
plt.ylabel('eta')
plt.title('Field enhancement')
plt.legend(['MoM', '2-param fit', 'MoM fit', 'Raizer'])

#%%
plt.figure(2)
plt.clf()
plt.plot(Larr,lam3,'x-')
#plt.plot(Larr,lam3,'o-')
#plt.plot(Larr,MoM.rod_cap_lincharge_minimal(Larr),'--')
plt.plot(Larr, MoM.rod_cap_lincharge_neck2(Larr))
#def lincharge_general(La1,a,b):
#    return a*La1 + b*np.sqrt(La1)
#popt,_ = curve_fit(lincharge_general,Larr[2:]-2,lam3[2:])
#plt.plot(Larr[2:],lincharge_general(Larr[2:]-2,*popt))

#%%
if config=='cap':
    err1 = np.array([s[4] for s in stuff]) # 3% for large L/a
    err2 = np.array([s[5] for s in stuff]) # <1% for most, especially large L/a
    l = np.array([s[1][0] for s in stuff])
    a = np.array([s[2][0] for s in stuff])
    plt.figure(3)
    plt.clf()
    plt.plot(Larr,err1)
    plt.plot(Larr,err2)
    # Looks like 1/(x+a)**2 is the way to go
    plt.figure(4)
    plt.clf()
    plt.plot(Larr,l,'x-')
    plt.plot(Larr,MoM.rod_cap_l(Larr))

#%%
def _distance_general(La,a,b,c):
    return a + b/(La + c)
popt,_ = curve_fit(_distance_general,Larr,l)

#%%
plt.figure(5)
plt.clf()
line,=plt.plot(Larr,eta,'x-')
line.set_label(r'MoM $\eta$')
line,=plt.plot(Larr,MoM.field_enhancement(Larr,config),'-')
line.set_label(r'Fitted $\eta$')
if config=='open':
    # line,=plt.plot(Larr,E,'x-')
    line.set_label(r'$E(x=a)$')
    line,=plt.plot(Larr,MoM.field_enhancement(Larr,'open_at_1'),'-')
    line.set_label(r'Fitted $E(x=a)$')
plt.grid(True)
plt.xlabel('Rod length L/a')
plt.ylabel('Field enhancement at the tip')
plt.title('Rod shape = '+config)
plt.legend()

if config=='open':
    plt.figure(6)
    plt.clf()
    plt.plot(Larr,loc,'x-')
    plt.plot(Larr,MoM.open_rod_Emax_x(Larr))
    plt.grid(True)
    plt.xlabel('Rod length L/a')
    plt.ylabel('Distance of Emax from the tip /a')
    plt.title('Rod shape = '+config)

#%%
L = 10
config = 'cap'
zb, rb = MoM.rod(L,config,dmin=2e-2,dmax=0.25,dxch=0.01,threfine=5,verbose=1)
c = MoM.CylSymConductor(zb,rb,Nsub=5,verbose=1)
c.fill_matrix()
#c.fill_matrix_slow(symmetric=True)
c.solve()
c.Ee = 1
c.Q = np.array([0])


#%% Plot of the shape
plt.figure(7)
plt.clf()
plt.plot(c.zb,c.rb,'x-')

#%% Plot of the charge density
fig2=plt.figure(8)
fig2.clear()
plt.plot(np.cumsum(c.d), c.q/c.area,'x-')
plt.grid(True)
plt.title('Charge density in uniform field')
plt.xlabel('Length along surface')
plt.ylabel('sigma')
fig2.canvas.draw()
fig2.show()

#%% Approximate theory of capped-rod field
def hemisphereE(z):
    """Field of a hemisphere. Returns ratio E/E1, where the radius a=1, E1=rho_s/(2 epsilon_0)
    is the field of a plane with same surface charge density.
    z is from the center, so z = x+a."""
    zm=np.abs(z-1)
    zp=np.sqrt(z**2+1)
    E = (1/2/z**2)*((z**2-1)*(1/zm-1/zp) + zp-zm)
    E[np.abs(z)<1e-5]=-.5
    return E

def eta_fun(La):
    """Field enhancement factor for a long rod, with open ends, approximate"""
    return La/(np.log(La)+2)

def rodE(z,La):
    """Field of a rod of radius a=1 and length La"""
    eta = eta_fun(La)
    E = eta*np.ones(z.shape)
    ii = (z>1) & (z<=La)
    E[ii]=eta/z[ii]
    ii = z>La
    E[ii]=La*eta/z[ii]**2
    return E

def Estreamer(x,Ee,Es,a,L):
    E0 = Ee-Es
    delta = 0.5 # not a parameter   
    E1 = eta_fun(L/a)*E0/delta
    return Es + E0*rodE(x/a+1,L/a) + E1*hemisphereE(x/a+1)

#%% Field on the axis, inside the conductor
# This section is obsolete, use c.E_axis!!!
c.Ee = 1
c.Q = np.array([0])
Np=10000
#zpb=np.linspace(L0/2-10,L0/2+10,Np)
zpb=np.linspace(L-1,L+L,Np)
dzp=zpb[1:]-zpb[:-1]
zpc=(zpb[:-1]+zpb[1:])/2
phipb=np.zeros(Np)
Epb=np.zeros(Np)
for k in range(Np):
    phipb[k]=c.pot_axis(zpb[k])
    Epb[k]=c.E_axis(zpb[k])
Epc=(phipb[:-1]-phipb[1:])/dzp
fig3 = plt.figure(9)
plt.clf()
plt.loglog(zpc-L,Epc)
plt.loglog(zpb-L,Epb)
plt.grid(True)
plt.title('N={}, E field'.format(c.N))
plt.xlabel('z')
plt.ylabel('E')
#eta = L/(np.log(L)+2)
delta = 0.5
Ea = Estreamer(zpb-L,1,0,1,L) #:eta/delta/(zpb-L+1)**2 + eta/(zpb-L+1)
Ea[zpb<L]=0
plt.loglog(zpb-L,Ea)
fig3.canvas.draw()
fig3.show()

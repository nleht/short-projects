#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 15 11:17:05 2016

Method of moments
-----------------

Setup and solve a cylindrically-symmetric system of conductors
in a uniform (axis-directed) external field.

Allows multiple conductors (arbitrary partitioning of the conductor) and has the function
short_circuit(connect), where "connect" is of format [0,0,1,1,1,2]
which means 0,1 are connected group 0; 2,3,4 are group 1 and 5 is group 2.

To start:
    
1. See the documentation for function "rod"

2. Try

>>> eta, l, x, E, l_err, conductor = rod_cap_field_enhancement_and_width(L=10, extra_info=True)
>>> plt.plot(x, E+1) # the field on the axis

and see the source code for this function

See also demos in files mom_demo_1.py and mom_demo_2.py.

@author: Nikolai G. Lehtinen
"""


#%% Obligatory imports
import numpy as np
import scipy.special
import scipy.linalg

print('*** Method of Moments ***')

def non_uniform_grid(L, dmax, dmin, dchange=0.1):
    """Element widths distributed between dmax (in the beginning) to dmin (in the end)"""
    assert dmax>=dmin and dchange>0 and dchange<.5 and L>=0
    if L==0:
        return np.array([0])
    if dmax==dmin:
        return np.linspace(0, L, int(np.ceil(L/dmin)))
    dmax1 = min(dmax, dchange*L/2+dmin)
    a = dmax1; b = (dmax1-dmin)/L**2; c = np.sqrt(a/b) # 1/sqrt(1-dmin/dmax1)*L > L
    d = 2*np.sqrt(a*b)
    N2a = np.log((c+L)/(c-L))/d
    N2 = int(np.ceil(N2a))
    n = np.arange(N2+1)*N2a/N2
    e = np.exp(d*n)
    return c*(e-1)/(1+e)

def rod(L, config, dmin=0.02, dmax=0.5, hole=1e-6, dxch=0.1, threfine=1, verbose=0):
    """
    Set up the elements of a rod of radius 1 and tip-to-tip length 2*L.
    The shape of the ends is given by string `config` in ['open','flat','cap'].
    
    
    Options
    -------
    dmin : 
        minimum element width, to be used close to the edges
    dmax : 
        maximum element width, to be used in the middle of the rod
    hole :
        a space left at the tips not covered by metal
    dxch :
        how much the element can change to the neighbor
    threfine :
        TBD
    verbose : default = 0
        extra output
        
    Returns
    -------
    zb, rb :
        Coordinates of the boundaries of intervals in cylindrical coordinates z, r describing the rod
    
    Example
    -------
    >>> import method_of_moments as MoM
    >>> L = 5 # half-length of the rod of radius 1
    >>> zb, rb = MoM.rod(L, 'cap')
    >>> conductor = MoM.CylSymConductor(zb, rb) # set up the conductor system
    >>> conductor.solve() # find the capacitance matrix etc.
    >>> plt.figure()
    >>> conductor.Ee = 1; conductor.Q = np.array([0.]); # unit external field, no charge
    >>> plt.plot(conductor.zc, conductor.q/conductor.zw) # plot linear charge density
    >>> plt.figure()
    >>> conductor.Ee = 0; conductor.Q = np.array([1.]); # no external field, put unit charge on the rod
    >>> plt.plot(conductor.zc, conductor.q/conductor.zw) # plot linear charge density
    """
    # After closing class docstring, there should be one blank line to
    # separate following codes (according to PEP257).
    # But for function, method and module, there should be no blank lines
    # after closing the docstring.
    if config=='flat' or config=='open':
        z2 = non_uniform_grid(L, dmax, dmin, dchange=dxch)[1:]
    elif config=='cap':
        z2 = non_uniform_grid(L-1, dmax, dmin, dchange=dxch)[1:]
    Ns = 2*len(z2) # Number of elements on the side surface
    zs = np.hstack((-np.flip(z2,axis=0),0,z2)) 
    if config == 'flat':
        Nf = int(np.ceil(1/dmin))
        zb = np.hstack((-L*np.ones((Nf+1,)),zs[1:-1],L*np.ones((Nf+1,))))
        rb = np.hstack((np.linspace(hole,1,Nf+1), np.ones((Ns-1,)), np.linspace(1,hole,Nf+1)))
    elif config == 'open':
        zb = zs
        rb = np.ones((Ns+1,))
    elif config == 'cap':
        thi = np.pi/2 - non_uniform_grid(np.pi/2-hole, dmin, dmin/threfine, dchange=dxch)
        th = np.flip(thi,axis=0)
        #th = np.pi/2*np.linspace(hole,1,Nf+1)
        #thi = np.flip(th,axis=0)
        if Ns==0:
            zb = np.hstack((-L+1-np.cos(th)[:-1],L-1+np.cos(thi)))
            rb = np.hstack((np.sin(th)[:-1],np.sin(thi)))
        else:
            zb = np.hstack((-L+1-np.cos(th),zs[1:-1],L-1+np.cos(thi)))
            rb = np.hstack((np.sin(th),np.ones((Ns-1,)),np.sin(thi)))
    else:
        raise Exception('unknown config')
    if verbose>0:
        print('Created surface of a rod with',len(zb)-1,'elements')
    if verbose>2:
        assert((np.diff(zb)>0).all())
        print('max dz =',np.max(np.diff(zb)),'; required =',dmax)
    return zb, rb

#%% Fitting functions, most obtained by fitting for L/a=1...100 (see mom_demo.py)

def _eta_general(La, a, b, c, d):
    return La/(a + b*np.log(La) + d*La) + c

_eta_popt = {'open_at_max':[ 1.99520243,  0.4693339 , -0.29420058, 0],
        'open_at_1':[ 2.61435934,  0.57057689, -0.19949766, 0],
        'open_at_0':[ 2.2107412 ,  0.55298292, -0.49221851, 0],
        #'cap':[0.74114183, 0.13050014, 0, 0], # up to L/a = 10000
        #'flat':[1.31267623e+00, 2.70419609e-01, 0, 0]}
        'cap':[ 4.81357144e-01,  1.85893730e-01, 0, -3.83927372e-04], # only for L/a<=100
        'flat':[1.31267623e+00, 2.70419609e-01, 0, 6.81834025e-04]} # accurate for L/a<=100

def field_enhancement(L, config):
    """The maximum field (which is at the tip of the rod) for 'cap' and 'flat' configurations.
    For 'open' configuration, the options are 'open_at_'+x, where x is '0','1', or 'max', because
    the max is not at the tip. The location of max for 'open' configuration is given by function
    rod_open_Emax_x.
    
    Important:
        1. L is the HALF of the rod length; the radius is 1.
        2. The external field is NOT included, add 1 to get the total.
        
    The Raizer value for 'cap' configuration is 2+0.56*(2*L)**0.92
    [Y. P. Raizer, "Gas discharge physics" (1991), p. 356]
    """
    return _eta_general(L, *(_eta_popt[config]))

def field_enhancement_cap_Raizer(L):
    return 2+0.56*(2*L)**0.92

def _distance_general(La, a, b, c):
    return a + b/(La + c)

def rod_open_Emax_x(L):
    "Location of the maximum field for open rod"
    popt = [0.36547253, 0.39891079, 0.30721206]
    return _distance_general(L, *popt)

def rod_cap_a(L):
    "Fit the field as eta/(1+x/a)**2, for distances 0<x<1"
    popt = [ 0.99945376, -1.30673327,  2.19141186]
    return _distance_general(L, *popt)

def rod_cap_l(L):
    "Fit the field as eta/(1+x/l), for distances 0<x<1"
    popt = [ 0.39553749, -0.58940288,  2.30673538]
    return _distance_general(L,*popt)

def _lincharge_general(La0, a, b):
    La = np.asarray(La0)
    res = np.zeros(La.shape)
    res[La>0]=a*La[La>0] + b*np.sqrt(La[La>0])
    return res[()]
    
def rod_cap_lincharge_minimal(La):
    """Estimate of the typical linear charge λ near the end of the 'cap' rod
    (namely, at neck z=L-1), obtained from dλ/dz in the middle of the rod. I.e.,
    the answer is (L-1)dλ/dz. The theoretical (Landau/Lifshits) value is
    [Liu et al, 2012, doi:10.1103/PhysRevLett.109.025002]:
        2*pi*(L-1)/(log(2L)-1)
    """
    popt = [0.84194973, 5.77357578]
    return _lincharge_general(La-1,*popt)
def rod_cap_lincharge_neck(La):
    "Linear charge at the neck (distance of 1 from tip)"
    popt = [2.70492454, 7.43102822]
    return _lincharge_general(La-1,*popt)
def rod_cap_lincharge_neck2(La):
    "Linear charge at distance of 2 from tip"
    popt = [1.94527123, 4.97408695]
    return _lincharge_general(La-2,*popt)

#%% Auxiliary functions

def _ave(x):
    return (x[1:]+x[:-1])/2

def _mom(zo,zs,ro,rs,wo,ws):
    "Private function, eliminate repetitive code"
    # *o are observation points (first index), *s are source points (second index)
    #zo=_ave(zbo); ro=_ave(rbo); wo = ro/nco
    #zs=_ave(zbs); rs=_ave(rbs); ws = rs/ncs
    # weight (wo, ws) is proportional to r, i.e., area
    # must have sum(weight1D over each element)==1
    zl,rk = np.meshgrid(zs,ro)
    rl,zk = np.meshgrid(rs,zo)
    dz = zl-zk # dz[k,l]==zs[l]-zo[k]
    dz2 = dz**2
    Ap = (rl+rk)**2+dz2 # maximum distance between points on #l and #k, squared
    # m=4*rl*rk/r2 # argument of the elliptic integral of 1st kind K(m)
    m1 = ((rl-rk)**2+dz2)/Ap # 1-m
    # for a thin ring of radius r=rl located at z=zl and unit charge,
    # this formula gives the electrostatic potential at r=rk and z=zk:
    M = scipy.special.ellipkm1(m1)/np.sqrt(Ap)/(2*np.pi**2)
    return M*np.outer(wo,ws)

def _partitioning(i):
    "From a piece number create the partitioning matrix. E.g., from [0,0,1,1] create [[1,1,0,0],[0,0,1,1]]"
    i = np.array(i)
    assert(issubclass(i.dtype.type, np.integer) and np.min(i)==0 and i.size==len(i))
    N = len(i)
    P = np.max(i)+1
    Pmat = np.zeros((P, N))
    Pmat[i, range(N)] = 1
    return Pmat

def _pieces(xb):
    """From a NaN-separated array of interval boundaries, extract
    indices of boundaries and piece number.
    The output piece_number is like the 'connect' argument for CylSymConductor.__init__()"""
    isleft = np.isfinite(xb) & np.isfinite(np.roll(xb,-1))
    isleft[-1] = False
    isright = np.roll(isleft,1)
    piece_number = np.cumsum(~np.isfinite(xb))[isleft]
    num_pieces = piece_number[-1]+1
    return isleft, isright, piece_number, num_pieces

#%% A cylidrically symmetric conductor consisting of conical belt elements
# The finite elements are given by (TODO)
# Basis (expansion) functions u[k]=
# Testing (weighting) functions w[k]=
class CylSymConductor:
    """
    A cylidrically symmetric conductor consisting of conical belt elements.
    
    Methods
    -------
    __init__ :
        Create
    solve :
        Solve
    """
    # After closing class docstring, there should be one blank line to
    # separate following codes (according to PEP257).
    # But for function, method and module, there should be no blank lines
    # after closing the docstring.
    
    def __init__(self, zb, rb, Nsub=3, connect=None, verbose=0):
        """
        Create a cylidrically symmetric conductor consisting of conical belt elements.
        
        Parameters
        ----------
        zb, rb : 1D arrays
            z- and ρ-coordinates of boundaries between elements, pieces separated by NaNs for separate conductors
        
        Optional
        --------
        Nsub : int or int array
            number of sub-intervals in each element for refinement
        connect : int array
            E.g., [0, 0, 1, 1, 1] means the first two pieces are connected and so are the last three pieces
        verbose : default = 0
            if >0, increase the debugging output
        """
        self.verbose = verbose
        assert not any(rb <= 0) # there should not be any points on the axis!
        self.zb = zb; self.rb = rb # save for possible diagnostics (not used in calculations)
        # Parse the conductor pieces
        i1, i2, piece, P = _pieces(zb)
        Pmat = _partitioning(piece)
        zb1 = zb[i1]; self.zb1 = zb1
        zb2 = zb[i2]; self.zb2 = zb2
        # sanity check
        i1r,_,_,_ = _pieces(rb)
        assert(np.array_equal(i1,i1r))
        rb1 = rb[i1]; self.rb1 = rb1
        rb2 = rb[i2]; self.rb2 = rb2
        self.N = len(zb1)
        # Secondary partitioning (initial)
        if connect is None:
            self.Pmat = Pmat
        else:
            assert(len(connect)==P)
            self.Pmat = np.dot(_partitioning(connect), Pmat)
        self.Np = self.Pmat.shape[0] # Number of connected groups (pieces)
        Nsub = np.asarray(Nsub,dtype=int)
        if Nsub.shape==():
            self.Nsub=Nsub*np.ones(self.N, dtype=int)
            # how many sub-elements in each element
        else:
            if Nsub.shape != (self.N,):
                print(Nsub.shape, self.N)
                raise Exception('Wrong size of Nsub array')
            self.Nsub = Nsub
        # Central values of the coordinates (denoted with "c")
        # These are arrays of length N
        self.rc = (rb1+rb2)/2.0
        self.zc = (zb1+zb2)/2.0
        # The "widths" of the "belt" in z and r directions
        self.zw = zb2-zb1
        rw = rb2-rb1
        # Total width of the belt
        self.d = np.sqrt(self.zw**2+rw**2)
        # Area of the "belt"
        self.area = 2*np.pi*self.rc*self.d
        # Sine and cosine of the angle of the "belt"
        self.s = rw/self.d
        self.c = self.zw/self.d
        # Prepare for the calculations
        self.equations = None
        self.Ee = 0
        self.Q = np.zeros((self.Np,))
    def diagonal_matrix_element(self,i):
        "Potential of #i at position of #i, M[i,i]"
        z1 = self.zb1[i]; z2 = self.zb2[i]; r1 = self.rb1[i]; r2 = self.rb2[i]; rc = self.rc[i]
        # Observation points
        Neo = self.Nsub[i]+1 # avoid singularities, choose a different number of intervals!
        zbo=np.linspace(z1,z2,Neo+1)
        rbo=np.linspace(r1,r2,Neo+1)
        zo = _ave(zbo); ro = _ave(rbo)
        wo = ro/(rc*Neo)
        # Source points
        Nes = self.Nsub[i]
        zbs=np.linspace(z1,z2,Nes+1)
        rbs=np.linspace(r1,r2,Nes+1)
        zs = _ave(zbs); rs = _ave(rbs)
        ws = rs/(rc*Nes)
        return np.sum(_mom(zo,zs,ro,rs,wo,ws))
    def fill_matrix(self):
        """The created matrix M is always symmetric.
        M[k,l] is the potential of unit charge on #l at surface of #k"""
        # Create subdivided arrays
        Nt = np.sum(self.Nsub) # total number of sub-intervals
        # "*1" are the left boundaries and "*2" are the right boundaries
        zbe1 = np.zeros((Nt,)); zbe2 = np.zeros((Nt,))
        rbe1 = np.zeros((Nt,)); rbe2 = np.zeros((Nt,))
        ib = np.hstack((0, np.cumsum(self.Nsub))) # boundaries between elements
        for k in range(self.N):
            Nb = self.Nsub[k]+1
            zbe = np.linspace(self.zb1[k], self.zb2[k], Nb)
            i = slice(ib[k], ib[k+1])
            zbe1[i] = zbe[:-1]
            zbe2[i] = zbe[1:]
            rbe = np.linspace(self.rb1[k],self.rb2[k],Nb)
            rbe1[i] = rbe[:-1]
            rbe2[i] = rbe[1:]
        ze = (zbe1+zbe2)/2; re = (rbe1+rbe2)/2
        # Normalizing coefficient, weight must sum to 1 in each interval
        we = re/(np.repeat(self.rc, self.Nsub)*np.repeat(self.Nsub, self.Nsub))
        c_inv_sub = _mom(ze,ze,re,re,we,we)
        np.fill_diagonal(c_inv_sub,0) # replace infinity with zero on diagonal
        integrated_c_inv_sub = np.cumsum(np.cumsum(c_inv_sub,axis=0),axis=1)
        # Mint misses the first row and column which should be zero
        # Undersample Mint so that we get boundaries between elements
        ii = ib[1:]-1
        integrated_c_inv = np.zeros((self.N+1,self.N+1))
        integrated_c_inv[1:,1:] = integrated_c_inv_sub[ii[:,np.newaxis],ii]
        # The full inverse capacitance matrix
        C_inv = np.diff(np.diff(integrated_c_inv,axis=0),axis=1)
        for i in range(self.N):
            C_inv[i,i]=self.diagonal_matrix_element(i)
        if self.verbose>0:
            print('The matrix is symmetric to accuracy =',
                  np.max(np.abs(C_inv-C_inv.T))/np.max(np.abs(C_inv)),flush=True)    
        # Now, include the partitioning. See equation (3) from README.md
        self.equations = np.block([[C_inv,      self.Pmat.T],
                                   [self.Pmat, np.zeros((self.Np,self.Np))]])
        # A slice view (no extra memory), i.e. discard memory of C_inv
        self.C_inv = self.equations[:self.N,:self.N]
    def solve(self):
        if self.equations is None:
            self.fill_matrix()
        # 1. Solve for unit charges, no external field
        rhs = np.vstack((np.zeros((self.N,self.Np)),np.eye(self.Np)))
        sol = np.linalg.solve(self.equations, rhs)
        # The unpartitioning matrix for charges (element charges in zero field):
        self.q_from_Q = sol[:-self.Np,:] # same as P_φ^T, see below
        # Mutual inverse capacitance matrix, Ncon x Ncon, "grouped" C_inv
        self.Cp_inv = -sol[-self.Np:,:]
        # 2. Solve for unit external field, no charges
        # The RHS is [minus external potential -phie (N), conductor charges Q (P)]
        # The solution is [element charges q(N), minus conductor potentials -Phi (Np)]
        rhs = np.hstack((self.zc, np.zeros((self.Np,))))
        sol = np.linalg.solve(self.equations, rhs)
        # This is 1D array of length N, formula is a bit involved
        self.q_from_Ee = sol[:-self.Np] # element charges for unit field, zero total charges
        # Conductor potentials for zero total charges, unit field, Phi_from_Ee = (q_from_Q)^T phie
        # where phie = -zc. Note that (q_from_Q)^T == P_φ from the README.md
        # The analgous role for q_from_Ee is played by a more complicated matrix,
        # i.e. q_from_Ee = C (-phie + phi) = C (-I_N + P^T P_φ) phie where I_N is a unit N x N matrix
        self.Phi_from_Ee = -sol[-self.Np:]
    @property
    def q(self):
        if self.q_from_Ee is None:
            if self.verbose>0:
                print('The conductor was not solved, solving now!')
            self.solve()
        return (self.q_from_Q @ self.Q) + self.q_from_Ee*self.Ee
    @property
    def Phi(self):
        "Potential of each peace"
        return self.Cp_inv @ self.Q + self.Phi_from_Ee*self.Ee
    def short_circuit(self, connect, change=True):
        """Dynamic short-circuiting of conductor groups. Charge transfer can be inferred as
        difference with previous value of self.Q (this function changes it)."""
        if self.q_from_Ee is None:
            if self.verbose>0:
                print('The conductor was not solved, solving now!')
            self.solve()
        assert(len(connect)==self.Np)
        Pmat = _partitioning(connect) # groups of conductors
        Ngroups = Pmat.shape[0]
        Qgroups = Pmat @ self.Q # group charges
        # Solve exactly the same equations but with
        #   Cp_inv instead of C_inv
        #   Phi_from_Ee instead of phie
        #   Qgroups instead of Q
        equations_groups = np.block([[self.Cp_inv, Pmat.T], [Pmat, np.zeros((Ngroups,Ngroups))]])
        rhs = np.hstack((-self.Phi_from_Ee, Qgroups))
        sol = np.linalg.solve(equations_groups, rhs)
        Qnew = sol[:self.Np] # new conductor charges
        if change:
            self.Q = Qnew
        Phi_groups = -sol[self.Np:] # New common potential
        return Qnew, Phi_groups # optional
    
    # The field calculations
    def _axis_aux(self,z):
        """Elimination of code repetition for axis field calculations"""
        a = z-self.zc
        # Axis t is along the slant height of the cone
        # t==0 at the point where the direction to obs. point is perp to slant
        # This minimal distance is self._p
        self._p  = a*self.s + self.rc*self.c
        t0 = a*self.c - self.rc*self.s
        self._t1 = -self.d/2 - t0
        self._t2 =  self.d/2 - t0
        # np.log(|p|) is needed in _aux_integral
        self._lp = np.log(self._p**2)/2
        self._lp[self._p==0] = 0
    def _aux_integral(self, t):
        "Eliminate repeating code"
        R = np.sqrt(t**2 + self._p**2)
        assert all(R>0) # sanity check
        s = np.sign(t)
        I0 = s*(np.log(s*t+R) - self._lp) # Integral of 1/R dt
        return R, I0
    def _pax_aux(self, t):
        """Used in the potential of the conical belt"""
        R, I0 = self._aux_integral(t)
        return self.s*R + self._p*self.c*I0
    def pot_axis_element(self, z):
        """Potential of a conical belt with unit charge, on the axis only, vectorized"""
        # the surface of the conical frustum
        # The potential is integral from t1 to t2 of
        # dq / (4*pi*R), R = sqrt(p**2+t**2)
        # We assume uniform surface charge density, total is unit charge Q/eps0 = 1
        # Then dq = 2*pi*r(t)*dt/area, where r(t) = rc + s*(t+t0) = c*p + s*t is the ring radius
        # Thus, potential = 1/(2*area) * int_t1^t2 (c*p + s*t)/R dt
        self._axis_aux(z) # calculate self._p, self._t1, self._t2
        return 1/(2*self.area)*(self._pax_aux(self._t2) - self._pax_aux(self._t1))
    def _Eax_aux(self, t):
        """Used in the field of the conical belt"""
        R, I0 = self._aux_integral(t)
        return 2*self.s*self.c*t/R + (self.c**2 - self.s**2)*self._p/R - self.c*self.s*I0
    def E_axis_element(self,z):
        """Field of the same belt as in 'pot_axis_element', on the axis, vectorized"""
        # int_t1^t2 dq*z(t)/(4*pi*R**3)
        # where dq, R are given above (in pot_axis), and z(t) = s*p - c*t
        # Thus, E = 1/(2*area) * int_t1^t2 (c*p + s*t)*(s*p - c*t)/R**3 dt
        self._axis_aux(z) # calculate self._p, self._t1,self._t2   
        return 1/(2*self.area)*(self._Eax_aux(self._t2) - self._Eax_aux(self._t1))
    def pot_axis(self,z):
        return np.sum(self.q*self.pot_axis_element(z))
    def E_axis(self,z):
        return np.sum(self.q*self.E_axis_element(z))
       
#%% A simple application
from scipy.optimize import curve_fit
def rod_cap_field_enhancement_and_width(L, max_r=1.5, verbose=0, extra_info=False):
    """Calculation for the function "field_enhancement" above, for 'cap' configuration only.
    This is a boiled-down function "analyze_rod" in file mom_demo_1.py for details.
    Parameters:
        L - half-length of a capped cylinder of radius a=1
    Options:
        max_r - the fitting to find Naidis E-field width is done in [0,max_r] interval,
            NOTE: it looks like the fitting rod_cap_l was produced with max_r=1.5
        extra_info - if set, return the error in l-fitting and the conductor object.
    """
    # Create a rod
    zb,rb = rod(L,'cap',dmin=0.02,dmax=L/50, hole=1e-6, threfine=10,verbose=verbose)
    conductor = CylSymConductor(zb,rb,Nsub=3,verbose=verbose)
    if verbose>0:
        print('Created a conductor with',conductor.N,'elements',flush=True)
    conductor.solve()
    conductor.Ee = 1
    conductor.Q = np.array([0.]) # was default
    # There is no meaning in resolving x with precision higher than minimum element width
    dx = np.min(conductor.d)
    x=np.arange(0, max_r, dx) # go only out to the radius
    # The field is NOT including the external field!
    E = np.array([conductor.E_axis(x1+L) for x1 in x])
    imax = np.nanargmax(E)
    eta = E[imax]
    #% Fit the Naidis curve
    def Naidis(z,l):
        return 1/(1+z/l)
    xs = x[imax:]
    Es = E[imax:]/eta
    popt,_ = curve_fit(Naidis,xs,Es)
    l = popt[0]
    l_err = np.max(np.abs(Naidis(xs,l)-Es))
    if extra_info:
        xextra = np.flip(np.arange(0,-.5,-dx))[:-1]
        Eextra = np.array([conductor.E_axis(x1+L) for x1 in xextra])
        xfull = np.hstack((xextra,x))
        Efull = np.hstack((Eextra,E))
        return eta,l,xfull,Efull,l_err,conductor
    else:
        return eta,l
    
        
        
        

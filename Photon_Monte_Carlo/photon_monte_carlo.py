#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 12:22:08 2021

A simple Monte Carlo model of photons.

@author: nle003
"""

#%% Preliminaries
import time
import numpy as np
import scipy.constants as phys

# Some constants
# Molecule density at ground level
Nm0 = phys.physical_constants['Loschmidt constant (273.15 K, 101.325 kPa)'][0]
# Atmosphere: atomic number, number of atoms in molecule, percent fraction of molecules
atmosphere = ((7, 2, .78), (8, 2, .21), (18, 1, .01))
def Zm_power(p):
    "Average nuclear charge power p in an atmospheric molecule"
    return sum(num_atoms*Z**p*frac for Z, num_atoms, frac in atmosphere)
Zm = Zm_power(1) # average number of electrons in a molecule (incl. Ar)
Zm2 = Zm_power(2)
Zm5 = Zm_power(5) # average Z^5 per molecule (incl. Ar)
rclas = phys.physical_constants['classical electron radius'][0]
mc2eV = phys.m_e*phys.c**2/phys.e

# Photon cross-sections
def cross_section_photoionization(k):
    """Take into account 1% of Ar. This is pulled from my old MATLAB code.
    (21.17) in Heitler"""
    # Eion = Z**2/(2*137**2)
    g = k + 1
    p = np.sqrt(g**2 - 1)
    return Zm5*4*np.pi*rclas**2/137**4./k**5.*p**3.*(
        4/3 + g*(g-2)/(g+1)*(1-1./(2.*g*p)*np.log((g+p)/(g-p))) )

def cross_section_compton(En):
    "From photon_r.pdf -- total Compton cross section σ per molecule"
    return Zm*2*np.pi*rclas**2*( (1+En)/En**2 * ((2+2*En)/(1+2*En)-np.log(1+2*En)/En) +
             np.log(1+2*En)/(2*En) - (1+3*En)/(1+2*En)**2 )

def cross_section_klein_nishina(En, th):
    "Differential cross section dσ/dΩ per electron"
    EEp = 1 + En*(1-np.cos(th))
    xs = 0.5*rclas**2*(EEp+1/EEp-np.sin(th)**2)/EEp**2
    return xs

def cross_section_pair_Motz(k):
    "Motz et al, 3D-0000, from my old MATLAB code"
    k = np.asarray(k)
    ismall = (k<3)&(k>2)
    ibig = (k>=3)
    ks = k[ismall]
    kb = k[ibig]
    e=(ks-2)/(ks+2)
    e1 = 2.*e/(1+np.sqrt(1-e**2))
    sigs = 2*np.pi/3*((ks-2)/ks)**3.*(
        1+(1./2.)*e1+(23./40.)*e1**2+(11./60.)*e1**3 + (29./960.)*e1**4)
    log2k = np.log(2.*kb)
    sigb = 28./9.*log2k - 218./27. + (2./kb)**2.*(
        6.*log2k - 7./2. + 2./3.*log2k**3 - log2k**2 - np.pi**2/3.*log2k + 
        np.pi**2/6. + 2*1.2020569) \
        - (2./kb)**4.*(3./16.*log2k + 1./8.) - \
            (2./kb)**6.*(29./(9.*256)*log2k - 77./(27.*512))
    sig = np.zeros(k.shape)
    sig[ismall] = sigs
    sig[ibig] = sigb
    return sig[()]*Zm2*rclas**2/137


def cross_section_pair_ER(k):
    "ER case from my old MATLAB code"
    k = np.asarray(k)
    sig=28./9.*np.log(2.*k)-218./27.
    sig[sig<0]=0 # No pair production at k<2
    sig=sig*Zm2*rclas**2/137;
    return sig[()]

# The "ER" version may be a bit faster, but not very different
cross_section_pair = cross_section_pair_Motz

def cross_section_compton_choose_angle_energy(En):
    "Random angle and corresponding energy from Klein-Nishina formula"
    remain = np.ones(En.shape, dtype=bool)
    costh = np.full(En.shape, fill_value=np.nan)
    while remain.any():
        N = np.sum(remain) # do not use "sum"
        E = En[remain]
        x = 1-(np.exp(np.random.rand(N)*np.log(1+2*E))-1)/E # from 1/EEp
        EEp = 1 + E*(1-x)
        prob = (EEp+1/EEp-1+x**2)/EEp/2 # relative to 1/EEp
        reject = np.random.rand(N) > prob
        costh[remain] = x
        remain[remain] = reject
    Enp = En/(1 + En*(1-costh))
    return costh, Enp

        
#%% The photon swarm class
class PhotonSwarm:
    status_dict = {'prop':0, 'sat':1, 'ground':2, 'phec':3, 'pair':4}
    def __init__(self, N, En, h, Hsat, costh_interval=[-1, 1], compton_deposition=True,
                 propagate_pair=False, Hatm=8400):
        """An isotropic instantaneous monoenergetic swarm of N photons
        of energy En in units of mc2 at altitude h (in m)."""
        # The initial distribution
        self.valid = np.ones((N,), dtype=bool)
        self.status = np.zeros((N,), dtype=int) # all have status 'fly'
        self.t = np.zeros((N,))
        self.r = np.zeros((3,N))
        self.r[2] = h
        # Isotropic distribution
        c0, c1 = costh_interval
        self.costh = np.random.rand(N)*(c1-c0) + c0   # cos(th)
        self.sinth = np.sqrt(1-self.costh**2)           # sin(th)
        ph = np.random.rand(N)*2*np.pi
        self.sinph = np.sin(ph)
        self.cosph = np.cos(ph)
        self.En = En*np.ones((N,))
        self.compton_deposition = compton_deposition
        if compton_deposition:
            self.compton_altitudes = np.array([])
            self.compton_energies = np.array([])
        self.propagate_pair = propagate_pair
        self.Hatm = Hatm    # atmosphere scale height
        self.Hsat = Hsat    # satellite altitude (at which the simulation ends)

    @property
    def N(self):
        return np.sum(self.valid) # do not use "sum"
    @property
    def px(self):
        return self.En*self.sinth*self.cosph
    @property
    def py(self):
        return self.En*self.sinth*self.sinph
    @property
    def pz(self):
        return self.En*self.costh
    @property
    def p(self):
        "Momentum in units of mc2"
        return np.vstack((self.px, self.py, self.pz))

    def rotate(self, costhscat):
        # The new direction if the original direction was in z-direction
        phscat = np.random.rand(self.N)*2*np.pi
        cosphscat = np.cos(phscat)
        sinphscat = np.sqrt(1-cosphscat**2)
        sts = np.sqrt(1-costhscat**2)
        x0, y0, z0 = sts*cosphscat, sts*sinphscat, costhscat
        # 1. Rotate by theta around y-axis
        costh = self.costh[self.valid]
        sinth = self.sinth[self.valid]
        cosph = self.cosph[self.valid]
        sinph = self.sinph[self.valid]
        x1 = x0*costh + z0*sinth
        y1 = y0
        z1 = -x0*sinth + z0*costh
        # 2. Rotate by phi around z-axis
        x2 = x1*cosph - y1*sinph
        y2 = x1*sinph + y1*cosph
        z2 = z1
        p2 = np.sqrt(x2**2 + y2**2)
        # Update the angles
        self.costh[self.valid] = z2
        # ph = np.arctan2(y2, x2)
        self.sinph[self.valid] = y2/p2 # np.sin(ph)
        self.cosph[self.valid] = x2/p2 # np.cos(ph)
        self.sinth[self.valid] = p2    # same as np.sqrt(1-z2**2)

    def step(self):
        #######################################################################
        # Cross-sections
        En = self.En[self.valid]
        xs_phot = cross_section_photoionization(En)
        xs_compt = cross_section_compton(En)
        xs_pair = cross_section_pair(En)
        xs = xs_phot + xs_compt + xs_pair
        # insanity check
        assert (xs_phot>=0).all() and (xs_compt>=0).all() and (xs_pair>=0).all()
        #######################################################################
        # Choose the path length in m*(m^-3) = m^-2
        z0 = self.r[2][self.valid]
        costh = self.costh[self.valid]
        lnorm = -np.log(np.random.rand(self.N))/xs
        # Determine the next collision point from integral(Nm(r)dr)=lnorm
        expzH = np.exp(-z0/self.Hatm) - lnorm*costh/(self.Hatm*Nm0)
        # Determine if we left the simulation domain
        out_up = (expzH < np.exp(-self.Hsat/self.Hatm))
        znew = np.full((self.N,), fill_value=np.nan)
        znew[~out_up] = -np.log(expzH[~out_up])*self.Hatm
        out_down = (znew<=0) # nans give False
        # Determine the intersection with satellite altitude for particles that left upwards
        l_up = (self.Hsat-z0[out_up])/costh[out_up]
        assert (l_up>0).all()
        sinth = self.sinth[self.valid]
        xdir = sinth*self.cosph[self.valid]
        ydir = sinth*self.sinph[self.valid]
        valid_out_up = self.valid.copy()
        valid_out_up[self.valid] = out_up
        self.r[0][valid_out_up] += l_up*xdir[out_up]
        self.r[1][valid_out_up] += l_up*ydir[out_up]
        self.r[2][valid_out_up] = self.Hsat
        self.t[valid_out_up] += l_up
        self.status[valid_out_up] = self.status_dict['sat']
        # Photons lost at the lower boundary -- analogous
        l_down = -z0[out_down]/costh[out_down]
        assert (l_down>0).all()
        valid_out_down = self.valid.copy()
        valid_out_down[self.valid] = out_down
        self.r[0][valid_out_down] += l_down*xdir[out_down]
        self.r[1][valid_out_down] += l_down*ydir[out_down]
        self.r[2][valid_out_down] = 0
        self.t[valid_out_down] += l_down
        self.status[valid_out_down] = self.status_dict['ground']
        # The rest which are still flying to the next collision point
        v = (~out_down) & (~out_up)
        l = (znew[v] - z0[v])/costh[v]
        assert (l>0).all()
        valid_v = self.valid.copy()
        valid_v[self.valid] = v
        self.r[0][valid_v] += l*xdir[v]
        self.r[1][valid_v] += l*ydir[v]
        self.r[2][valid_v] = znew[v] # += l*self.costh[v]
        self.t[valid_v] += l
        self.valid[self.valid] = v
        #######################################################################
        # The photoionization (and pair production)
        xs_phot = xs_phot[v]
        xs_compt = xs_compt[v]
        xs_pair = xs_pair[v]
        xs = xs_phot + xs_pair + xs_compt
        process = np.random.rand(self.N)
        is_phot = (process < xs_phot/xs)
        absorbed = (process < (xs_phot+xs_pair)/xs)
        is_pair = absorbed & (~is_phot)
        valid_is_phot = self.valid.copy()
        valid_is_phot[self.valid] = is_phot
        self.status[valid_is_phot] = self.status_dict['phec']
        valid_is_pair = self.valid.copy()
        valid_is_pair[self.valid] = is_pair
        self.status[valid_is_pair] = self.status_dict['pair']
        self.valid[self.valid] = ~absorbed
        #######################################################################
        # Change of angle and energy for Compton
        xs_compt = xs_compt[~absorbed]
        En = self.En[self.valid] # it is saved
        costhscat, Enp = cross_section_compton_choose_angle_energy(En)
        self.En[self.valid] = Enp
        if self.compton_deposition:
            self.compton_altitudes = np.hstack((self.compton_altitudes, self.r[2][self.valid]))
            self.compton_energies = np.hstack((self.compton_energies, En-Enp))
        self.rotate(costhscat)
        #######################################################################
        # Create a pair of photons from a photon absorbed with pair production
        if self.propagate_pair:
            # Add two 511k photons for each pair. The pair-absorbed photon stays absorbed.
            Npair = np.sum(is_pair)
            assert np.sum(valid_is_pair)==Npair
            ph = np.random.rand(Npair)*2*np.pi
            sinph = np.sin(ph)
            cosph = np.cos(ph)
            costh = np.random.rand(Npair)*2-1
            sinth = np.sqrt(1-costh**2)
            # Adjust the energy deposited by the "pair-absorbed" photon
            self.En[valid_is_pair] -= 2
            # Extend the arrays
            self.valid = np.hstack((self.valid, np.ones((2*Npair,), dtype=bool)))
            self.status = np.hstack((self.status, np.zeros((2*Npair,), dtype=int)))
            self.En = np.hstack((self.En, np.ones((2*Npair,))))
            self.sinph = np.hstack((self.sinph, sinph, -sinph))
            self.cosph = np.hstack((self.cosph, cosph, -cosph))
            self.sinth = np.hstack((self.sinth, sinth, sinth))
            self.costh = np.hstack((self.costh, costh, -costh))
            rpair = self.r[:,valid_is_pair]
            self.r = np.hstack((self.r, rpair, rpair))
            tpair = self.t[valid_is_pair]
            self.t = np.hstack((self.t, tpair, tpair))
        # final sanity check
        assert (self.valid==(self.status==self.status_dict['prop'])).all()
    def run(self, verbose=True):
        nsteps = 0
        t0 = time.time()
        tx = t0
        N = self.N
        while N>0:
            self.step()
            N = self.N
            t1 = time.time()
            if verbose:
                print('(#{:d}:{:d}/{:d}ph, {:.2f}s)'.format(
                    nsteps, N, len(self.valid), t1-t0), flush=True, end=(', ' if N>0 else '\n'))
            nsteps += 1
            t0 = t1
        if verbose:
            print('This run took {:.3f} min'.format((t1-tx)/60))

#%%
if __name__=='__main__':
    from matplotlib import pyplot as plt
    #% Radiation diagram
    plt.figure(1)
    plt.clf()
    th = np.arange(361)*np.pi/180
    for En_eV in [.1e6, 1e6, 10e6]:
        xs = cross_section_klein_nishina(En_eV/mc2eV, th)
        line, = plt.plot(xs*np.cos(th), xs*np.sin(th))
        line.set_label('$E='+str(En_eV/1e6)+'$ MeV')
    plt.axis('equal')
    plt.legend()
    plt.grid(True)
    plt.title('Compton radiation pattern')
    
    #% Test the angle choice
    plt.figure(2)
    plt.clf()
    En_eV = 1e6
    En = En_eV/mc2eV
    N = int(1e6)
    costh, Enp  = cross_section_compton_choose_angle_energy(En*np.ones((N,)))
    val, bins, _ = plt.hist(costh, bins=100)
    dcth = np.diff(bins)
    cth = (bins[1:]+bins[:-1])/2
    xsnorm1 = val/dcth/N # gives one when integrated over cos(th) (sampled at points cth)
    th = np.arccos(cth)
    xstot = cross_section_compton(En)/Zm # total cross-section per electron
    xsnorm = cross_section_klein_nishina(En, th)*2*np.pi/xstot # gives one when integrated over cos(th)
    plt.xlabel(r'$\cos\theta$')
    plt.ylabel('$d\sigma/d\Omega$, arb. units')
    plt.title('Differential Compton cross-sec. for $E='+str(En_eV/1e6)+'$ MeV')
    deviation = np.max(np.abs(xsnorm1/xsnorm-1)) # an error we made
    print('Worst deviation =', deviation)
    line, = plt.plot(cth, xsnorm*dcth*N)
    line.set_label('Normalized Klein-Nishina cross-sec.')
    plt.legend()
    # plt.figure(11)
    # plt.clf()
    # plt.plot(xsnorm/xsnorm1)
    
    #% Plot the total xsec
    plt.figure(3)
    plt.clf()
    En = np.logspace(-2, 2, 1000)/mc2eV*1e6
    plt.loglog(En*mc2eV/1e6, cross_section_compton(En))
    plt.loglog(En*mc2eV/1e6, cross_section_photoionization(En))
    plt.loglog(En*mc2eV/1e6, cross_section_pair(En))
    plt.legend(['Compton', 'Photoionization', 'Pair production'])
    plt.xlabel(r'$E$, MeV')
    plt.ylabel('$\sigma$, m$^2$')
    plt.grid(True)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 01:39:20 2021

Storage class creator

@author: nleht
"""

import numpy as np

#%% So that the storage may be of different types
class _FieldAccess:
    "Descriptor"
    def __init__(self, name, dtype, address):
        self.name = name
        self.dtype = dtype
        self.address = address
    def __get__(self, obj, objtype):
        #print('Getting', self.name, 'of type', self.dtype, 'at address', self.address)
        return obj._stor[self.dtype][self.address]
    def __set__(self, obj, val):
        #print('Setting', self.name, 'of type', self.dtype, 'at address',
        #      self.address, 'with value', val)
        obj._stor[self.dtype][self.address] = val

def storage_class_creator(classname, nfields, fields):
    class _Storage(object):
        __slots__ = ['_stor', '_n'] + [f[0] for f in fields]
        def __init__(self, arrs):
            self._stor = {}
            self._n = None
            for arr, nfield in zip(arrs, nfields):
                dtype, m = nfield
                assert arr.shape[0] == m
                n = arr.shape[1]
                if self._n is None:
                    self._n = n
                assert self._n == n
                # Need to confirm that the dtype is compatible!
                assert arr.dtype is np.array([], dtype=dtype).dtype
                self._stor[dtype] = arr
        def __len__(self):
            return self._n
        def __getitem__(self, i):
            return self.__class__([self._stor[dtype][:,i] for dtype in self._stor.keys()])
        def __setitem__(self, i, v):
            assert isinstance(v, self.__class__)
            for k in self._stor.keys():
                self._stor[dtype][:,i] = v._stor[dtype]
        def __add__(self, other):
            return self.__class__([
                np.hstack((self._stor[dtype], other._stor[dtype]))
                for dtype in self._stor.keys()])
        def fill(self, **kws):
            for k, v in kws.items():
                setattr(self, k, v)
        @classmethod
        def zeros(cls, n):
            return cls([np.zeros((m, n), dtype=dtype) for dtype, m in nfields])
    for name, dtype, address in fields:
        setattr(_Storage, name, _FieldAccess(name, dtype, address))
    _Storage.__name__ = classname
    _Storage.__qualname__ = classname
    return _Storage

if __name__=='__main__':
    try:
        from utils import colorize
    except:
        def colorize(s, **kw): return s
    # Can have intersecting fields! E.g., here x, y, and z are duplicated as r:
    PhotonStorage = storage_class_creator('PhotonStorage', [(float, 3), (bool, 1), (int, 1)],
        [('x', float, 0), ('y', float, 1), ('z', float, 2),
         ('r', float, slice(0, 3)), ('valid', bool, 0), ('status', int, 0)])
    s1 = PhotonStorage.zeros(10)
    s2 = PhotonStorage.zeros(5)
    s1.fill(x=1, y=2, z=3, status=7)
    s1.valid = 4 # sets to True
    try:
        s1.v = 4 # gives AttributeError
    except AttributeError as e:
        print('Caught AttributeError:', colorize(str(e), color='red', bold=True))
    s2.x = 10
    s3 = s1 + s2
    print(s3.x)
    print(s3.valid)
    t = s3[s3.valid]
    s3[3:13]=t
    # How to access the descriptor internals? s3.v.address will not work, because s3.v already
    # something else
    print(type(s3).__dict__['valid'].address)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 10:33:45 2017

Some utilities of questionable usability (some may be reinventions of a wheel)

@author: nle003
"""

import os
import pickle
import numbers
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import io
import scipy.constants as phys
import scipy.special as spec
import scipy.interpolate
import scipy.optimize
import sys
import pkgutil
import operator
import functools

####################################################################################################
#%% A few general-purpose routines

def make_dir(dirname, verbose=False):
    "Create directory *dirname* with subdirectories, warn if create, do nothing if exists"
    if not os.path.isdir(dirname):
        print(red('Warning:'), 'Creating directory', dirname)
        os.makedirs(dirname)
    elif verbose:
        print(blue('Info:'), 'Using directory', dirname)

def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif isinstance(obj, np.ndarray):
        size += obj.nbytes
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size

def imports():
    "List all the imported modules"
    import types
    for name, val in globals().items():
        if isinstance(val, types.ModuleType):
            print(val.__name__,'as',name)

def reimport(module):
    """Does not always work. Please try built-in IPython commands, or (still better)
    restart the kernel"""
    import importlib
    for name in dir(module):
        if name[0:2]!='__': delattr(module, name)
    importlib.reload(module)
    
def submodules(package):
    return list(p.name for p in pkgutil.iter_modules(package.__path__))

####################################################################################################
#%% Numerics

def index_interval(xlim1,xlim2,xarr):
    "Index in xarr corresponding to [xlim1,xlim2], but extend by one (for good interpolation)"
    i = (xarr>=xlim1) & (xarr<=xlim2)
    i[1:] |= i[:-1]; i[:-1] |= i[1:]
    return i

def line_interp2d(x,y,xp,yp,zp):
    """Workaround around clumsy 2D interpolation in scipy.
    Stolen from https://stackoverflow.com/a/47233198"""
    shape=x.shape
    assert y.shape==shape
    f = scipy.interpolate.interp2d(xp, yp, zp, bounds_error=True)
    return scipy.interpolate.dfitpack.bispeu(
            f.tck[0], f.tck[1], f.tck[2], f.tck[3], f.tck[4],
            x.flat, y.flat)[0].reshape(shape)

def ones_as(x):
    "Scalar or vector ones"
    return np.ones(np.asarray(x).shape)[()]

def zeros_as(x):
    "Scalar or vector zeros"
    return np.zeros(np.asarray(x).shape)[()]

def anti_diff(arr0,darr):
    "The inverse operation to ``np.diff`` for 1D arrays"
    return np.cumsum(np.hstack((arr0,darr)))

def ave(x):
    """Useful for plotting histograms:
        plt.plot(ave(edges),hist,'.')
    See also: duplicate, meandrize
    """
    return (x[1:]+x[:-1])/2

def aver(x, axis=None, oper='A'):
    "By analogy with numpy.diff -- apply an operator to neigboring nodes, default operator is averaging"
    av = lambda x,y: (x+y)/2
    if isinstance(oper, str):
        oper = {'+':operator.add, '*':operator.mul, '|':operator.or_, '&':operator.and_, 'A':av}[oper]
    if np.isscalar(x):
        raise Exception('operation aver cannot be applied to '+str(x))
    ndim = np.ndim(x)
    assert ndim>0
    if axis is None:
        assert ndim==1
        axis = 0
    i1 = tuple(slice(1,None) if a==axis else slice(None) for a in range(ndim))
    i2 = tuple(slice(None,-1) if a==axis else slice(None) for a in range(ndim))
    return oper(x[i1], x[i2])

# NOTE: these two function are not really needed, use drawstyle='steps-post' keyword to plot
def duplicate(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    An alternative to drawstyle='steps-post' keyword
    """
    return np.tile(np.array([x]).T,2).flatten()

def meandrize(x):
    """Useful for plotting histograms:
        plt.plot(meandrize(edges),duplicate(hist))
    """
    return np.hstack((x[0],duplicate(x[1:-1]),x[-1]))

def make_dispatch(arg,boundaries):
    "Auxiliary function to be used with numpy.choose"
    tmp=np.asarray(arg)
    res=np.zeros(tmp.shape,dtype=np.int)
    for b in boundaries:
        res += (tmp>=b).astype(np.int)
    return res

def dispatch(arg,boundaries,results):
    """Choose results depending on the arg belonging to an interval
    defined by boundaries"""
    return np.choose(make_dispatch(arg,boundaries),results)

def map_to_ndarray(func,ndarr,depth=1,params=(),keywords={}):
    """A more intuitive version of np.vectorize()"""
    if depth==0:
        return func(ndarr,*params,**keywords)
    elif depth>0:
        return np.array([map_to_ndarray(func,subarr,depth=depth-1,
                                        params=params,keywords=keywords)
                        for subarr in ndarr])
    else:
        raise Exception('depth')

def solve_nonlinear_equation_system(funcs, grid, xtol, ftol, vectorized=None, order=None,
                                    profiling=False, debug=False):
    """Search for multiple solutions of a nonlinear system with the resolution given by the grid.
    
    Inputs
    ------
    
    funcs : tuple (or array_like) of functions of length ndim
        ndim is the number of equations, equal to the number of unknowns
    grid : tuple of 1D arrays (of length ndim)
        Each 1D array gives the grid along that dimension
    xtol : tuple of floats (of length ndim)
        To distinguish different solutions -- note that it is different from scipy.optimize.root etc. `xtol`
    ftol : tuple of floats (of length ndim)
        To check validity of solution. Again, different from scipy.optimize.root etc. `ftol`
        
    Options
    -------
    
    order : tuple (or array_like) of int of length ndim
        order of computational complexity of functions, from easiest to hardest, default=range(ndim)
    vectorized : tuple (or array_like) of bool of length ndim
        additional optimization if some of the functions are vectorized, default=array of False
    profiling : bool
        Return extra argument 'info', default=False
    debug : bool
        For developer debugging, do not use, default=False
        
    Outputs
    -------
    
    sols : array of tuples of length ndim
        Array can be empty if no solutions are found.
    info : dict, optional (if profiling=True)
    """
    ndim = len(funcs)
    # check the inputs
    if vectorized is None:
        vectorized = (False,)*ndim
    if order is None:
        order = tuple(range(ndim)) # Default order
    assert len(order)==ndim and len(grid)==ndim and len(vectorized)==ndim
    ncalls = np.zeros((ndim,), dtype=int) # for profiling
    # The algorightm auxiliary functions -- we avooid extra packages
    def box_has_zero(fres):
        "fres changes sign in this box"
        iis = tuple(aver(fres, axis=a, oper='*')<0 for a in range(ndim))
        t = ()
        for axis, res in enumerate(iis):
            for a in range(ndim):
                if a != axis: res = aver(res, axis=a, oper='|')
            t += (res,)
        return functools.reduce(operator.or_, t)
    def spread(haszero):
        "For True at grid cell, make it True for nodes belonging to that cell"
        prev = haszero
        for axis in range(ndim):
            res = np.pad(prev, tuple((0,1) if a==axis else (0,0) for a in range(ndim)))
            i1 = tuple(slice(1,None) if a==axis else slice(None) for a in range(ndim))
            res[i1] |= prev
            prev = res
        return prev
    def box_info(i, grid, f=None, multidim=True):
        "For debugging mostly"
        lower = tuple(grid[a][i[a]] for a in range(ndim))
        upper = tuple(grid[a][i[a]+1] for a in range(ndim))
        xlimits = tuple(zip(lower, upper))
        xcenter = tuple((b[0]+b[1])/2 for b in xlimits)
        bounds = (lower, upper)
        xcorners = np.meshgrid(*xlimits, indexing='ij') # The coordinates of the corners
        # Extract values f at the corners of this box, without calculating f(xcorners) [if vectorized]
        index_limits = tuple([i[a], i[a]+1] for a in range(ndim))
        icorners = np.meshgrid(*index_limits, indexing='ij') # in the same format as xcorners
        corner_indices = list(zip(*(c.flat for c in icorners))) # in format useful for indexing
        # Values of f at the corners, i.e., self.apply(xcorners)
        if f is not None:
            if multidim:
                fcorners = tuple(np.array([f[a][ii] for ii in corner_indices]).reshape((2,)*ndim)
                                 for a in range(ndim))
            else:
                fcorners = np.array([f[ii] for ii in corner_indices]).reshape((2,)*ndim) 
        else:
            fcorners = None
        return xlimits, bounds, xcenter, xcorners, fcorners
    def apply_func_num(num, x, vectorized=False):
        "Apply a scalar function by its index in 'funcs' to a vector of arrays x"
        nonlocal ncalls
        scalar = np.isscalar(x[0])
        ncalls[num] += (1 if scalar else x[0].size)
        if scalar or vectorized:
            return funcs[num](x)
        # x is an array
        s = x[0].shape
        # Non-vectorized calculation
        return np.array([funcs[num](args) for args in zip(*(x1.flat for x1 in x))]).reshape(s)
    def vfunc(x):
        "Compose a vector function from scalar functions"
        return tuple(apply_func_num(a, x) for a in range(ndim))
    # Find the indices of the boxes in which the functions change sign, with some optimization magic
    shape = tuple(len(x_) for x_ in grid)
    x = np.meshgrid(*grid, indexing='ij')
    f = []
    for axis in range(ndim):
        num = order[axis]
        #func = funcs[order[axis]]
        isvect = vectorized[num]
        if axis==0:
            fres = apply_func_num(num, x, vectorized=isvect) # full evaluation
            haszero = box_has_zero(fres)
        else:
            sp = spread(haszero)
            fres = np.full(shape, fill_value=np.nan)
            xonly = tuple(x1[sp] for x1 in x)
            fres[sp] = apply_func_num(num, xonly, vectorized=isvect) # sparing evaluation
            haszero &= box_has_zero(fres)
        f.append(fres)
    indices = list(zip(*np.where(haszero)))
    ncalls1 = ncalls.copy()
    # Go over the suspicious boxes and apply scipy methods to find solutions
    sols = []
    debug = False
    for i in indices:
        xlimits, bounds, xcenter, xcorners, fcorners = box_info(i, grid, f)
        if debug:
            print('\n***',i,'***\nBox limits =', xlimits,
                  '\nFcorners =', fcorners, '\nFcorners =', vfunc(xcorners), flush=True)
            print('center =', xcenter,'; fcenter =', vfunc(xcenter), flush=True)
        # Try first fsolve, then least_squares!
        sol, infodict, ier, mesg = scipy.optimize.fsolve(vfunc, xcenter, full_output=True)
        if ier != 1: # did not succeed
            # method=='least_squares':
            if not np.isfinite(fcorners).all(): continue
            if debug: print('Bounds =', bounds, flush=True)
            result = scipy.optimize.least_squares(vfunc, xcenter, bounds=bounds)
            sol = result.x
            # Check if it is valid solution
            fsol = vfunc(sol)
            valid_solution = functools.reduce(operator.and_, (np.abs(fsol[a])<ftol[a] for a in range(ndim)))
            if not valid_solution:
                continue
        # Check if 'sol' is unique
        is_new = True
        for sol_ in sols:
            different = functools.reduce(operator.or_, (np.abs(sol[a] - sol_[a]) > xtol[a] for a in range(ndim)))
            is_new = is_new and different # could use functools.reduce again here
        if is_new:
            sols.append(sol)
            if debug: print('sol =', sol, flush=True)
    if debug: print('Unique solutions:', sols, flush=True)
    if profiling:
        ncalls_extra = np.unique(ncalls-ncalls1)
        assert len(ncalls_extra)==1
        info = {'indices':indices, 'ncalls':ncalls1, 'ncalls_extra':ncalls_extra[0]}
        return sols, info
    return sols

#%%#################################################################################################
# Plotting

def fig_save(fig,fname):
    "Save figure `fig` into file `fname`"
    if isinstance(fig, numbers.Integral):
        fig = plt.figure(fig)
    with open(fname,'wb') as f:
        pickle.dump(fig,f)

def fig_open(fname, show=True):
    "Return figure stored in file `fname` in pickle format"
    with open(fname,'rb') as f:
        fig = pickle.load(f)
    if show:
        fig.show()
    return fig

def nice_figures(on=True):
    "Nice-looking figures (sort of)"
    if on:
        #frame_title = [0.13,0.14,0.84,0.77] # left, bottom, width, height
        #frame_notitle = [0.13,0.14,0.84,0.8] # left, bottom, width, height, no space for title
        # Default figure size is 6 x 4 for inline and 8 x 6 for TkAgg
        mpl.rcParams['figure.figsize'] = (6.0, 4.0)
        # Default font size is 10 for inline and 12 for TkAgg
        mpl.rcParams['font.size'] = 12.0 # this line gets ignored
        mpl.rcParams['figure.subplot.bottom'] = 0.14 # default: 0.11,
        #mpl.rcParams['figure.subplot.hspace'] = 0.2,
        mpl.rcParams['figure.subplot.left'] = 0.13 # default: 0.125,
        mpl.rcParams['figure.subplot.right'] = 0.97 # default: 0.9,
        mpl.rcParams['figure.subplot.top'] = 0.92 # default: 0.88
        #mpl.rcParams['figure.subplot.wspace'] = 0.2,
        mpl.rcParams['axes.grid'] = True # default: False,
        mpl.rcParams['axes.grid.axis'] = 'both' # default: 'both',
        mpl.rcParams['axes.grid.which'] = 'both' # default: 'major',
    else:
        # Restore defaults
        for k,v in mpl.rcParams.items():
            vd = mpl.rcParamsDefault[k]
            if v!=vd and k!='interactive':
                #print(k,v,vd)
                mpl.rcParams[k] = (vd.copy() if isinstance(vd,list) else vd)

def place_text(x, y, text, size=None, ax=None):
    "Determine the limits from x, y in [0,1] and scale and place at the correct coordinates"
    if size is None:
        size = 30
    if ax is None:
        ax = plt.gca()
    def convert(z01,sz,z):
        z0,z1 = z01
        if sz=='linear':
            zs = z0 + z*(z1-z0)
        elif sz=='log':
            zs = np.exp(np.log(z0)+z*np.log(z1/z0))
        else:
            raise Exception('scale')
        return zs
    plt.text(convert(ax.get_xlim(),ax.get_xscale(),x),
             convert(ax.get_ylim(),ax.get_yscale(),y),text,fontsize=size)

def place_letter(t, size=None, ax=None, loc=None):
    if loc is None:
        loc = 'se'
    if loc[0]=='s':
        ytext = 0.03
    elif loc[0]=='n':
        ytext = 0.85
    else:
        raise Exception('uknown south or north')
    if loc[1]=='e':
        xtext = 0.88
    elif loc[1]=='w':
        xtext = 0.02
    else:
        raise Exception('uknown east or west')
    place_text(xtext, ytext, '('+t+')', size=size, ax=ax)

def plt_no_offsets():
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)
    plt.gca().get_yaxis().get_major_formatter().set_useOffset(False)
    plt.draw()

def reset_colors():
    plt.gca().set_prop_cycle(None)

####################################################################################################
#%% Terminal colors and pretty printing
# Note that some of the functionality is provided by package print_color which may be installed with
# pip install -U print-color
# See https://www.digitalocean.com/community/tutorials/how-to-customize-your-bash-prompt-on-a-linux-vps
# \e -- same as \033 is escape char
# \e[ -- start of color info
# m\] -- end of color info
# Color info: 0,1,4 - normal,bold or underlined text; 3x -- foreground color; 4x -- background color, where
# x = 0-7 is black, red, green, yellow, blue, purple, cyan, white
# \[\e]0; -- start of the terminal title (not displayed in the normal prompt)
# \a\] -- end of the terminal title

__colors = {} # will not get visible imported by from utils import *
def colorful_print(use_colors=True):
    "Make a colorful terminal output. Use set_colors(False) to turn it off."
    global __colors
    if use_colors:
        __colors = {'black': '\x1b[30m',
                    'red': '\x1b[31m',
                    'green': '\x1b[32m',
                    'yellow': '\x1b[33m',
                    'blue': '\x1b[34m',
                    'magenta': '\x1b[35m',
                    'cyan': '\x1b[36m',
                    'white': '\x1b[37m',
                    'end': '\x1b[0m',
                    'bold': '\x1b[1m',
                    'underline': '\x1b[4m'}
    else:
        __colors = {'black':'', 'red':'', 'green':'', 'yellow':'',
                  'blue':'', 'magenta':'', 'cyan':'', 'white':'',
                  'end':'', 'bold':'', 'underline':''}

colorful_print()

def get_color(arg):
    "May be unnecessary"
    return __colors[arg]

def colorize(s,color=None,bold=False,underline=False):
    if color is not None:
        s = s + get_color('end')
        if bold:
            s = get_color('bold') + s
        if underline:
            s = get_color('underline') + s
        s = get_color(color) + s
    return s

def red(s):
    return colorize(s,color='red',bold=True)

def blue(s):
    return colorize(s,color='blue',bold=True)

def string(*a,**kw):
    "Python3-style print into a string"
    with io.StringIO() as s:
        kw.setdefault('file',s)
        kw.setdefault('end','')
        print(*a,**kw)
        tmp=s.getvalue()
    return tmp

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

def latex_float(f,digits=5):
    s = '-' if (f<0) else ''
    float_str = ('{0:.'+str(int(abs(digits)))+'g}').format(abs(f))
    if "e" in float_str:
        base, exponent = float_str.split("e")
        basef = '' if base=='1' else r'{:} \times'.format(base)
        return s + basef + r'10^{{{:}}}'.format(int(exponent))
    else:
        return s + float_str

def string_header(*a,color=None,bold=False,linewidth=None,fill='*',**print_kw):
    s = string(*a,**print_kw)
    if linewidth is None:
        linewidth=np.get_printoptions()['linewidth']
    if len(s)==0:
        s = fill*linewidth
    else:
        if len(s)>linewidth-2:
            return s
        n1 = (linewidth-2-len(s))//2
        n2 = linewidth-2-len(s)-n1
        s = fill*n1 + ' ' + s + ' ' + fill*n2
    return colorize(s,color=color,bold=bold)

def print_header(*a,**b):
    print(string_header(*a,**b))

def print_big_header(*a,color=None,bold=False,linewidth=None,fill='*',**print_kw):
    if linewidth is None:
        linewidth=np.get_printoptions()['linewidth']    
    print(get_color(color) + (get_color('bold') if bold else '') + fill*linewidth)
    print(string_header(*a,linewidth=linewidth,fill=fill,**print_kw))
    print(fill*linewidth + get_color('end'))
    
def nice_print(on=True):
    "NumPy output suited for the wide screen in Spyder"
    if on:
        np.set_printoptions(edgeitems=100, linewidth=120, threshold=10000)
    else:
        np.set_printoptions(edgeitems=3, linewidth=75, threshold=1000)

####################################################################################################
#%% Use unicode to simulate bold, italic, fraktur, double-struck, sans-serif, monospace etc.
# May be combine with 'colorize' defined above
__latin = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
__latin_decorated = {
    'bold' : '𝐀𝐁𝐂𝐃𝐄𝐅𝐆𝐇𝐈𝐉𝐊𝐋𝐌𝐍𝐎𝐏𝐐𝐑𝐒𝐓𝐔𝐕𝐖𝐗𝐘𝐙𝐚𝐛𝐜𝐝𝐞𝐟𝐠𝐡𝐢𝐣𝐤𝐥𝐦𝐧𝐨𝐩𝐪𝐫𝐬𝐭𝐮𝐯𝐰𝐱𝐲𝐳𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗',
    'italic' : '𝐴𝐵𝐶𝐷𝐸𝐹𝐺𝐻𝐼𝐽𝐾𝐿𝑀𝑁𝑂𝑃𝑄𝑅𝑆𝑇𝑈𝑉𝑊𝑋𝑌𝑍𝑎𝑏𝑐𝑑𝑒𝑓𝑔ℎ𝑖𝑗𝑘𝑙𝑚𝑛𝑜𝑝𝑞𝑟𝑠𝑡𝑢𝑣𝑤𝑥𝑦𝑧0123456789',
    'bold italic' : '𝑨𝑩𝑪𝑫𝑬𝑭𝑮𝑯𝑰𝑱𝑲𝑳𝑴𝑵𝑶𝑷𝑸𝑹𝑺𝑻𝑼𝑽𝑾𝑿𝒀𝒁𝒂𝒃𝒄𝒅𝒆𝒇𝒈𝒉𝒊𝒋𝒌𝒍𝒎𝒏𝒐𝒑𝒒𝒓𝒔𝒕𝒖𝒗𝒘𝒙𝒚𝒛𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗',
    'script' : '𝒜ℬ𝒞𝒟ℰℱ𝒢ℋℐ𝒥𝒦ℒℳ𝒩𝒪𝒫𝒬ℛ𝒮𝒯𝒰𝒱𝒲𝒳𝒴𝒵𝒶𝒷𝒸𝒹ℯ𝒻ℊ𝒽𝒾𝒿𝓀𝓁𝓂𝓃ℴ𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏0123456789',
    'script bold' : '𝓐𝓑𝓒𝓓𝓔𝓕𝓖𝓗𝓘𝓙𝓚𝓛𝓜𝓝𝓞𝓟𝓠𝓡𝓢𝓣𝓤𝓥𝓦𝓧𝓨𝓩𝓪𝓫𝓬𝓭𝓮𝓯𝓰𝓱𝓲𝓳𝓴𝓵𝓶𝓷𝓸𝓹𝓺𝓻𝓼𝓽𝓾𝓿𝔀𝔁𝔂𝔃𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗',
    'fraktur' : '𝔄𝔅ℭ𝔇𝔈𝔉𝔊ℌℑ𝔍𝔎𝔏𝔐𝔑𝔒𝔓𝔔ℜ𝔖𝔗𝔘𝔙𝔚𝔛𝔜ℨ𝔞𝔟𝔠𝔡𝔢𝔣𝔤𝔥𝔦𝔧𝔨𝔩𝔪𝔫𝔬𝔭𝔮𝔯𝔰𝔱𝔲𝔳𝔴𝔵𝔶𝔷0123456789',
    'double-struck' : '𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫𝟘𝟙𝟚𝟛𝟜𝟝𝟞𝟟𝟠𝟡',
    'fraktur bold' : '𝕬𝕭𝕮𝕯𝕰𝕱𝕲𝕳𝕴𝕵𝕶𝕷𝕸𝕹𝕺𝕻𝕼𝕽𝕾𝕿𝖀𝖁𝖂𝖃𝖄𝖅𝖆𝖇𝖈𝖉𝖊𝖋𝖌𝖍𝖎𝖏𝖐𝖑𝖒𝖓𝖔𝖕𝖖𝖗𝖘𝖙𝖚𝖛𝖜𝖝𝖞𝖟𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗',
    'sans-serif' : '𝖠𝖡𝖢𝖣𝖤𝖥𝖦𝖧𝖨𝖩𝖪𝖫𝖬𝖭𝖮𝖯𝖰𝖱𝖲𝖳𝖴𝖵𝖶𝖷𝖸𝖹𝖺𝖻𝖼𝖽𝖾𝖿𝗀𝗁𝗂𝗃𝗄𝗅𝗆𝗇𝗈𝗉𝗊𝗋𝗌𝗍𝗎𝗏𝗐𝗑𝗒𝗓𝟢𝟣𝟤𝟥𝟦𝟧𝟨𝟩𝟪𝟫',
    'sans-serif bold' : '𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭𝗮𝗯𝗰𝗱𝗲𝗳𝗴𝗵𝗶𝗷𝗸𝗹𝗺𝗻𝗼𝗽𝗾𝗿𝘀𝘁𝘂𝘃𝘄𝘅𝘆𝘇𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵',
    'sans-serif italic' : '𝘈𝘉𝘊𝘋𝘌𝘍𝘎𝘏𝘐𝘑𝘒𝘓𝘔𝘕𝘖𝘗𝘘𝘙𝘚𝘛𝘜𝘝𝘞𝘟𝘠𝘡𝘢𝘣𝘤𝘥𝘦𝘧𝘨𝘩𝘪𝘫𝘬𝘭𝘮𝘯𝘰𝘱𝘲𝘳𝘴𝘵𝘶𝘷𝘸𝘹𝘺𝘻𝟢𝟣𝟤𝟥𝟦𝟧𝟨𝟩𝟪𝟫',
    'sans-serif bold italic' : '𝘼𝘽𝘾𝘿𝙀𝙁𝙂𝙃𝙄𝙅𝙆𝙇𝙈𝙉𝙊𝙋𝙌𝙍𝙎𝙏𝙐𝙑𝙒𝙓𝙔𝙕𝙖𝙗𝙘𝙙𝙚𝙛𝙜𝙝𝙞𝙟𝙠𝙡𝙢𝙣𝙤𝙥𝙦𝙧𝙨𝙩𝙪𝙫𝙬𝙭𝙮𝙯𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵',
    'monospace' : '𝙰𝙱𝙲𝙳𝙴𝙵𝙶𝙷𝙸𝙹𝙺𝙻𝙼𝙽𝙾𝙿𝚀𝚁𝚂𝚃𝚄𝚅𝚆𝚇𝚈𝚉𝚊𝚋𝚌𝚍𝚎𝚏𝚐𝚑𝚒𝚓𝚔𝚕𝚖𝚗𝚘𝚙𝚚𝚛𝚜𝚝𝚞𝚟𝚠𝚡𝚢𝚣𝟶𝟷𝟸𝟹𝟺𝟻𝟼𝟽𝟾𝟿'}

def frakturize(s, family='', bold=False, italic=False):
    font = family + (' bold' if bold else '') + (' italic' if italic else '')
    if family == '':
        font = font[1:]
    if font not in __latin_decorated.keys():
        return s
    d = dict(zip(__latin, __latin_decorated[font]))
    return ''.join(d.get(c, c) for c in s)
